package com.example.diegopizzo.foodietest.config

import android.app.Application
import android.util.Log
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.config.koin.appModule
import com.example.diegopizzo.foodietest.config.koin.dbModule
import com.example.diegopizzo.foodietest.config.koin.networkModule
import com.example.diegopizzo.foodietest.config.koin.utilsModule
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessaging
import com.onesignal.OneSignal
import org.koin.android.ext.android.startKoin

class FoodieTestApplication : Application() {

    companion object {
        private const val TAG = "FoodieTestApplication"
    }

    override fun onCreate() {
        super.onCreate()
        startKoin(this, listOf(networkModule, appModule, dbModule, utilsModule))
        // OneSignal Initialization
        OneSignal.startInit(this)
            .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
            .unsubscribeWhenNotificationsAreDisabled(true)
            .init()
        retrieveFcmToken()
        subscribeTopic()
    }

    private fun retrieveFcmToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(TAG, "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                val token = task.result?.token

                // Log and toast
                val msg = getString(R.string.msg_token, token)
                Log.d(TAG, msg)
            })
    }

    private fun subscribeTopic() {
        FirebaseMessaging.getInstance().subscribeToTopic(getString(R.string.foodie_topic))
            .addOnCompleteListener { task ->
                var msg = getString(R.string.msg_subscribed)
                if (!task.isSuccessful) {
                    msg = getString(R.string.msg_subscribe_failed)
                }
                Log.d(TAG, msg)
            }
    }
}