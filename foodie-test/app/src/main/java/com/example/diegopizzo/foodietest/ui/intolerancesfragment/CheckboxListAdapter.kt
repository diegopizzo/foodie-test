package com.example.diegopizzo.foodietest.ui.intolerancesfragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import androidx.recyclerview.widget.RecyclerView
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.ui.utils.Intolerances


class CheckboxListAdapter(
    private val onFragmentInteractorListener: OnFragmentInteractorListener
) :
    RecyclerView.Adapter<CheckboxListAdapter.ViewHolder>() {
    private val intolerances: MutableList<String> = mutableListOf()
    var intolerancesFromDb: List<String> = mutableListOf()

    init {
        addItemsIntoTheList()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.intolerances_fragment_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return intolerances.size
    }


    private fun addItemsIntoTheList() {
        intolerances.addAll(Intolerances.values().map { intolerances -> intolerances.intolerance })
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val intoleranceText: String = intolerances[position]
        val intoleranceCheckbox = holder.checkBox
        intolerancesFromDb.map { intoleranceFromDb ->
            if (intoleranceFromDb == intoleranceText) intoleranceCheckbox.isChecked = true
        }
        intoleranceCheckbox.text = intoleranceText

        intoleranceCheckbox
            .setOnCheckedChangeListener { _, isChecked ->
                if (isChecked)
                    onFragmentInteractorListener.addIntolerance(Intolerances.getIntoleranceFromValue(intoleranceText))
                else
                    onFragmentInteractorListener.removeIntolerance(Intolerances.getIntoleranceFromValue(intoleranceText))
            }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val checkBox: CheckBox = itemView.findViewById(R.id.intoleranceCheckbox)
    }

    interface OnFragmentInteractorListener {
        fun addIntolerance(intolerances: Intolerances)
        fun removeIntolerance(intolerances: Intolerances)
    }
}