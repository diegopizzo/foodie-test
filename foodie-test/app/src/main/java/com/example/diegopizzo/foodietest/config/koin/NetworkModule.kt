package com.example.diegopizzo.foodietest.config.koin

import com.example.diegopizzo.foodietest.business.network.cache.FoodieStore
import com.example.diegopizzo.foodietest.business.network.interactor.FoodieInteractor
import com.example.diegopizzo.foodietest.business.network.service.FoodieService
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import org.koin.dsl.module.module
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

val networkModule = module {

    single { providesRetrofit(get()) }
    single { providesGsonConverterFactory(get()) }
    single { providesGson() }
    single { providesService(get()) }
    single { providesFoodieStore(get()) }
    single { providesFoodieServiceInteractor(get()) }

}

fun providesRetrofit(gsonConverterFactory: GsonConverterFactory): Retrofit {
    return Retrofit.Builder()
        .baseUrl(FoodieService.ENDPOINT)
        .addConverterFactory(gsonConverterFactory)
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()
}

fun providesGsonConverterFactory(gson: Gson): GsonConverterFactory {
    return GsonConverterFactory.create(gson)
}

fun providesGson(): Gson {
    return GsonBuilder().serializeNulls().create()
}

fun providesService(retrofit: Retrofit): FoodieService {
    return retrofit.create(FoodieService::class.java)
}

fun providesFoodieStore(foodieService: FoodieService): FoodieStore {
    return FoodieStore(foodieService)
}

fun providesFoodieServiceInteractor(foodieStore: FoodieStore): FoodieInteractor {
    return FoodieInteractor(foodieStore)
}