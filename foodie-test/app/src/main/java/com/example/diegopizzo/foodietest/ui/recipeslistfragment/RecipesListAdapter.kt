package com.example.diegopizzo.foodietest.ui.recipeslistfragment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import com.example.diegopizzo.foodietest.ui.utils.glideOptions
import com.example.diegopizzo.foodietest.ui.utils.titleTruncate
import kotlinx.android.synthetic.main.recipe_item_view_layout.view.*

class RecipesListAdapter(private val context: Context, private val listener: OnItemClickListener) :
    PagedListAdapter<Recipe, RecipesListAdapter.ViewHolder>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context: Context = parent.context
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view: View = inflater.inflate(R.layout.recipes_list_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val recipe = getItem(position)
        recipe?.let {
            holder.itemView.titleItemRecipe.text = titleTruncate(recipe.title)
            Glide.with(context).load(recipe.image).apply(glideOptions(context))
                .into(holder.itemView.imageItemRecipe)
            holder.setOnClickListener(recipe.id)
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun setOnClickListener(id: Int) {
            itemView.setOnClickListener {
                listener.onItemClick(id)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(id: Int)
    }

    companion object {
        /**
         * This diff callback informs the PagedListAdapter how to compute list differences when new
         * PagedLists arrive.
         */
        private val diffCallback = object : DiffUtil.ItemCallback<Recipe>() {
            override fun areItemsTheSame(oldItem: Recipe, newItem: Recipe): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: Recipe, newItem: Recipe): Boolean =
                oldItem == newItem
        }
    }
}