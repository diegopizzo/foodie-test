package com.example.diegopizzo.foodietest.business.db.datasource

import androidx.paging.ItemKeyedDataSource
import com.example.diegopizzo.foodietest.business.db.entity.RecipeEntity
import com.example.diegopizzo.foodietest.business.db.repository.RecipeRepository
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import com.example.diegopizzo.foodietest.business.network.interactor.FoodieInteractor
import io.reactivex.Single
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject

abstract class RecipeItemKeyedDataSourceBase : ItemKeyedDataSource<Int, Recipe>(), KoinComponent {

    protected val recipeRepository: RecipeRepository by inject()
    private val interactor: FoodieInteractor by inject()


    fun createRecipesSingle(entitiesList: List<RecipeEntity>): Single<List<Recipe>> {
        val singleListOfRecipe = entitiesList.map { interactor.getRecipeInformationById(it.idRecipe) }
        return Single.zipArray<Recipe, List<Recipe>>({ it.toList() as List<Recipe> }, singleListOfRecipe.toTypedArray())
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Recipe>) {
    }

    override fun getKey(item: Recipe) = item.id

}