package com.example.diegopizzo.foodietest.business.network.datamodel

import com.google.gson.annotations.Expose

data class ResponseResult(
    @Expose val results: List<Result>,
    @Expose val baseUri: String,
    @Expose val offset: Int,
    @Expose val number: Int
)