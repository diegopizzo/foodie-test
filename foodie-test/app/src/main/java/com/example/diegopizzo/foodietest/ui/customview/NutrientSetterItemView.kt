package com.example.diegopizzo.foodietest.ui.customview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.SeekBar
import androidx.cardview.widget.CardView
import com.example.diegopizzo.foodietest.R
import kotlinx.android.synthetic.main.nutrient_seek_bar_layout.view.*

class NutrientSetterItemView(context: Context, attrs: AttributeSet?) : CardView(context, attrs) {

    private lateinit var onChangeDefaultValueListener: OnChangeDefaultValueListener

    init {
        View.inflate(context, R.layout.nutrient_seek_bar_layout, this)
    }

    fun setNutrient(nutrient: String) {
        nutrientTextView.text = nutrient
    }

    fun setNutrientSeekBar(defaultValue: Int, maxValue: Int, minValue: Int) {
        nutrientSeekBar.progress = defaultValue
        nutrientSeekBar.max = maxValue
        nutrientSeekBar.min = minValue
        nutrientValueTextView.text = defaultValue.toString()

        nutrientSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                nutrientValueTextView.text = nutrientSeekBar.progress.toString()
                onChangeDefaultValueListener.onChangeDefaultValue(nutrientSeekBar.progress)
            }
        })
    }

    fun setOnChangeDefaultValueListener(onChangeDefaultValueListener: OnChangeDefaultValueListener) {
        this.onChangeDefaultValueListener = onChangeDefaultValueListener
    }

    interface OnChangeDefaultValueListener {
        fun onChangeDefaultValue(value: Int)
    }
}