package com.example.diegopizzo.foodietest.business.db.repository.impl

import android.os.AsyncTask
import com.example.diegopizzo.foodietest.business.db.dao.RecipeDao
import com.example.diegopizzo.foodietest.business.db.entity.IntoleranceEntity
import com.example.diegopizzo.foodietest.business.db.entity.RecipeEntity
import com.example.diegopizzo.foodietest.business.db.repository.RecipeRepository
import com.example.diegopizzo.foodietest.ui.utils.Intolerances
import io.reactivex.Completable
import io.reactivex.Single

class RecipeRepositoryImpl(private val recipeDao: RecipeDao) : RecipeRepository {

    override fun insertAll(vararg intoleranceEntity: IntoleranceEntity): Completable {
        return recipeDao.insertAll(*intoleranceEntity)
    }

    override fun insertAll(vararg recipeEntities: RecipeEntity): Completable {
        return recipeDao.insertAll(*recipeEntities)
    }

    override fun insertByIntoleranceName(name: Intolerances) {
        InsertAsyncTaskInsert(recipeDao).execute(name)
    }

    override fun loadIntolerances(): List<IntoleranceEntity> {
        return recipeDao.loadIntolerances()
    }

    override fun loadAllRecipesLiked(requestedLoadSize: Int): List<RecipeEntity> {
        return recipeDao.loadAllRecipesLiked(requestedLoadSize)
    }

    override fun loadAllRecipesLikedAfter(key: Int, requestedLoadSize: Int): List<RecipeEntity> {
        return recipeDao.loadAllRecipesLikedAfter(key, requestedLoadSize)
    }

    override fun loadAllRecipesNotLiked(requestedLoadSize: Int): List<RecipeEntity> {
        return recipeDao.loadAllRecipesNotLiked(requestedLoadSize)
    }

    override fun loadAllRecipesNotLikedAfter(key: Int, requestedLoadSize: Int): List<RecipeEntity> {
        return recipeDao.loadAllRecipesNotLikedAfter(key, requestedLoadSize)
    }

    override fun loadIntoleranceByName(intolerances: Intolerances): Single<IntoleranceEntity> {
        return recipeDao.loadIntoleranceByName(intolerances)
    }

    override fun deleteIntoleranceByName(name: Intolerances) {
        InsertAsyncTaskDelete(recipeDao).execute(name)
    }


    private class InsertAsyncTaskInsert internal constructor(private val mAsyncTaskDao: RecipeDao) :
        AsyncTask<Intolerances, Void, Void>() {

        override fun doInBackground(vararg params: Intolerances?): Void? {
            mAsyncTaskDao.insertByIntoleranceName(params[0]!!)
            return null
        }
    }

    private class InsertAsyncTaskDelete internal constructor(private val mAsyncTaskDao: RecipeDao) :
        AsyncTask<Intolerances, Void, Void>() {

        override fun doInBackground(vararg params: Intolerances?): Void? {
            mAsyncTaskDao.deleteIntoleranceByName(params[0]!!)
            return null
        }
    }
}