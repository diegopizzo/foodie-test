package com.example.diegopizzo.foodietest.ui.recipesbynutrientsfragment.recipesbynutrientssetter

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.databinding.RecipesByNutrientsSetterLayoutBinding
import com.example.diegopizzo.foodietest.ui.utils.Nutrients

class RecipesByNutrientsSetterFragment : Fragment() {

    private lateinit var binding: RecipesByNutrientsSetterLayoutBinding
    private val adapter = RecipesByNutrientsSetterAdapter(Nutrients.values().toList())
    private lateinit var onRecipesByNutrientsSetterFragmentInteractionListener: OnRecipesByNutrientsSetterFragmentInteractionListener

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.recipes_by_nutrients_setter_layout, container, false)
        setRecyclerView()
        setButtonClickListener()
        return binding.root
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        onRecipesByNutrientsSetterFragmentInteractionListener =
            activity as OnRecipesByNutrientsSetterFragmentInteractionListener
    }

    private fun setRecyclerView() {
        binding.nutrientsRecyclerView.layoutManager = LinearLayoutManager(context)
        binding.nutrientsRecyclerView.adapter = adapter
    }

    private fun setButtonClickListener() {
        binding.recipesByNutrientsSetterButton.setOnClickListener {
            onRecipesByNutrientsSetterFragmentInteractionListener.onRecipesByNutrientsSetterFragmentInteraction(
                adapter.getListOfNutrients().map { nutrient -> nutrient.defaultValue }.toIntArray()
            )
        }
    }

    companion object {
        const val TAG_RECIPES_BY_NUTRIENTS_SETTER_FRAGMENT = "recipesByNutrientsSetterFragment"
        const val NUTRIENTS_INT_ARRAY_KEY = "nutrientsArrayIntKey"

        //TODO fare un solo metodo centralizzato
        fun newInstance(bundle: Bundle?): RecipesByNutrientsSetterFragment {
            val recipesByNutrientsSetterFragment =
                RecipesByNutrientsSetterFragment()
            if (bundle != null) {
                recipesByNutrientsSetterFragment.arguments = bundle
            }
            return recipesByNutrientsSetterFragment
        }
    }

    interface OnRecipesByNutrientsSetterFragmentInteractionListener {
        fun onRecipesByNutrientsSetterFragmentInteraction(nutrientsIntArray: IntArray)
    }
}