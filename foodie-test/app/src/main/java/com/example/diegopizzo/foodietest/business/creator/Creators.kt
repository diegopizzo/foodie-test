package com.example.diegopizzo.foodietest.business.creator

import com.example.diegopizzo.foodietest.business.db.entity.RecipeEntity
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe

class Creators {

    fun fromRecipeDataModelToRecipeDbEntity(recipeDataModel: Recipe, isLiked: Boolean): RecipeEntity {
        return RecipeEntity(0, recipeDataModel.id, recipeDataModel.title, isLiked)
    }
}