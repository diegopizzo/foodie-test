package com.example.diegopizzo.foodietest.ui

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.business.network.datamodel.Result
import com.example.diegopizzo.foodietest.databinding.ActivityMainBinding
import com.example.diegopizzo.foodietest.ui.foodietestfragment.FoodieTestFragment
import com.example.diegopizzo.foodietest.ui.homefragment.HomeFragment
import com.example.diegopizzo.foodietest.ui.intolerancesfragment.IntolerancesFragment
import com.example.diegopizzo.foodietest.ui.recipedetailsactivity.RecipeDetailsActivity
import com.example.diegopizzo.foodietest.ui.recipedetailsactivity.RecipeDetailsActivity.Companion.RECIPE_ID_BUNDLE_KEY
import com.example.diegopizzo.foodietest.ui.recipesbynutrientsfragment.recipesbynutrientssetter.RecipesByNutrientsSetterFragment
import com.example.diegopizzo.foodietest.ui.recipesbynutrientsfragment.recipesbynutrientssetter.RecipesByNutrientsSetterFragment.Companion.NUTRIENTS_INT_ARRAY_KEY
import com.example.diegopizzo.foodietest.ui.recipesbynutrientsfragment.recipesbynutrientsvalues.RecipesByNutrientsValuesFragment
import com.example.diegopizzo.foodietest.ui.recipeslistfragment.RecipesListFragment
import com.example.diegopizzo.foodietest.ui.searchview.SearchHistoryDatabase
import com.example.diegopizzo.foodietest.ui.searchview.SearchViewAdapter
import com.example.diegopizzo.foodietest.ui.searchview.SearchViewModel
import com.google.android.material.navigation.NavigationView
import com.lapism.searchview.Search
import com.lapism.searchview.widget.SearchView
import org.koin.android.viewmodel.ext.android.viewModel


class MainActivity : AppCompatActivity(),
    RecipesByNutrientsSetterFragment.OnRecipesByNutrientsSetterFragmentInteractionListener {

    private lateinit var mDrawerLayout: DrawerLayout
    private lateinit var searchView: SearchView
    private val fragmentManager = supportFragmentManager
    private lateinit var binding: ActivityMainBinding
    private val searchViewModel by viewModel<SearchViewModel>()

    companion object {
        private const val TAG = "MainActivity"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.searchViewModel = searchViewModel
        if (!intent.getStringExtra(FoodieTestFragment.TAG_FOODIE_TEST_FRAGMENT).isNullOrEmpty()) {
            startFoodieTestFragment()
        } else {
            startHomeFragment()
        }
        setActionBar()
        setNavigationDrawerLayout()
        setSearchView()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                mDrawerLayout.openDrawer(GravityCompat.START)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        super.onCreateOptionsMenu(menu)
        return true
    }

    private fun startHomeFragment() {
        beginTransaction(
            fragmentManager.findFragmentByTag(HomeFragment.TAG_HOME_FRAGMENT) ?: HomeFragment.newInstance(null),
            HomeFragment.TAG_HOME_FRAGMENT, false
        )
    }


    private fun startFoodieTestFragment() {
        if (!isFragmentAlreadyExists(FoodieTestFragment.TAG_FOODIE_TEST_FRAGMENT)) {
            beginTransaction(
                fragmentManager.findFragmentByTag(FoodieTestFragment.TAG_FOODIE_TEST_FRAGMENT)
                    ?: FoodieTestFragment.newInstance(null),
                FoodieTestFragment.TAG_FOODIE_TEST_FRAGMENT, true
            )
        }
    }

    private fun startRecipesListFragment() {
        if (!isFragmentAlreadyExists(RecipesListFragment.TAG_RECIPES_LIST_FRAGMENT)) {
            beginTransaction(
                fragmentManager.findFragmentByTag(RecipesListFragment.TAG_RECIPES_LIST_FRAGMENT)
                    ?: RecipesListFragment.newInstance(null),
                RecipesListFragment.TAG_RECIPES_LIST_FRAGMENT, true
            )
        }
    }

    private fun startIntolerancesFragment() {
        if (!isFragmentAlreadyExists(IntolerancesFragment.TAG_INTOLERANCES_FRAGMENT)) {
            beginTransaction(
                fragmentManager.findFragmentByTag(IntolerancesFragment.TAG_INTOLERANCES_FRAGMENT)
                    ?: IntolerancesFragment.newInstance(null),
                IntolerancesFragment.TAG_INTOLERANCES_FRAGMENT, true
            )
        }
    }

    private fun startRecipesByNutrientsSetterFragment() {
        if (!isFragmentAlreadyExists(RecipesByNutrientsSetterFragment.TAG_RECIPES_BY_NUTRIENTS_SETTER_FRAGMENT)) {
            beginTransaction(
                fragmentManager.findFragmentByTag(RecipesByNutrientsSetterFragment.TAG_RECIPES_BY_NUTRIENTS_SETTER_FRAGMENT)
                    ?: RecipesByNutrientsSetterFragment.newInstance(null),
                RecipesByNutrientsSetterFragment.TAG_RECIPES_BY_NUTRIENTS_SETTER_FRAGMENT, true
            )
        }
    }

    private fun startRecipesByNutrientsValuesFragment(bundle: Bundle) {
        beginTransaction(
            fragmentManager.findFragmentByTag(RecipesByNutrientsValuesFragment.TAG_RECIPES_BY_NUTRIENTS_VALUES_FRAGMENT)
                ?: RecipesByNutrientsValuesFragment.newInstance(bundle),
            RecipesByNutrientsValuesFragment.TAG_RECIPES_BY_NUTRIENTS_VALUES_FRAGMENT, true
        )
    }

    private fun beginTransaction(fragment: Fragment, tag: String, isAddToBackStack: Boolean) {
        val fragmentTransaction =
            fragmentManager.beginTransaction().replace(R.id.fragment_container, fragment, tag)
        if (isAddToBackStack) fragmentTransaction.addToBackStack(tag).commit() else fragmentTransaction.commit()
    }

    private fun isFragmentAlreadyExists(tag: String): Boolean {
        return fragmentManager.findFragmentByTag(tag) != null
    }

    private fun setNavigationDrawerLayout() {
        mDrawerLayout = findViewById(R.id.drawer_layout)
        val navigationView: NavigationView = findViewById(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener { menuItem ->
            // set item as selected to persist highlight
            menuItem.isChecked = true
            // close drawer when item is tapped
            mDrawerLayout.closeDrawers()

            // Add code here to update the UI based on the item selected
            // For example, swap UI fragments here
            when (menuItem.itemId) {
                R.id.home -> startHomeFragment()
                R.id.foodie_test -> startFoodieTestFragment()
                R.id.view_your_list -> startRecipesListFragment()
                R.id.recipes_by_nutrients -> startRecipesByNutrientsSetterFragment()
            }

            true
        }
    }

    private fun setActionBar() {
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        val actionbar: ActionBar? = supportActionBar
        actionbar?.apply {
            setDisplayHomeAsUpEnabled(false)
        }
    }

    private fun setSearchView() {
        searchView = binding.searchView
        searchView.apply {
            setOnLogoClickListener { mDrawerLayout.openDrawer(GravityCompat.START) }
            setHint(getString(R.string.hint_search_view_search_recipes))
            setOnMenuClickListener { startIntolerancesFragment() }
            setMenuIcon(getDrawable(R.drawable.ic_intolerances_black_icon))
        }

        val mHistoryDatabase = SearchHistoryDatabase(this)

        val searchAdapter = SearchViewAdapter(this, object : SearchViewAdapter.OnSearchItemClickListener {
            override fun onSearchItemClick(recipe: Result) {
                mHistoryDatabase.addItemRecipe(Result(recipe.id, recipe.title, null))
                val intent = Intent(this@MainActivity, RecipeDetailsActivity::class.java).putExtra(
                    RECIPE_ID_BUNDLE_KEY,
                    recipe.id
                )
                startActivity(intent)
            }
        })
        searchView.adapter = searchAdapter


        searchView.setOnQueryTextListener(object : Search.OnQueryTextListener {
            override fun onQueryTextSubmit(query: CharSequence?): Boolean {
                searchViewModel.getRecipesByQuery(query.toString())
                return true
            }

            override fun onQueryTextChange(newText: CharSequence?) {
                if (!TextUtils.isEmpty(newText) && newText!!.count() > 3) {
                    searchViewModel.getRecipesByQuery(newText.toString())
                }
            }
        })

        searchViewModel.recipesListMutableLiveData
            .observe(this, Observer { t -> searchAdapter.addItemsIntoTheList(t) })
    }

    override fun onRecipesByNutrientsSetterFragmentInteraction(nutrientsIntArray: IntArray) {
        val bundle = Bundle()
        bundle.putIntArray(NUTRIENTS_INT_ARRAY_KEY, nutrientsIntArray)
        startRecipesByNutrientsValuesFragment(bundle)
    }
}