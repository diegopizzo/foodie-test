package com.example.diegopizzo.foodietest.business.network.cache

import com.example.diegopizzo.foodietest.business.network.datamodel.*
import com.example.diegopizzo.foodietest.business.network.service.FoodieService
import com.nytimes.android.external.store3.base.impl.Store
import com.nytimes.android.external.store3.base.impl.StoreBuilder
import io.reactivex.Single

class FoodieStore(private val foodieService: FoodieService) {

    companion object {
        private const val delimiter = ";"
    }

    private val searchRecipesStore: Store<ResponseResult, String>
    private val getSimilarRecipes: Store<Result, Int>
    private val getRandomRecipe: Store<ResponseRecipe, Int>
    private val getRecipeById: Store<Recipe, Int>
    private val getRecipesByNutrients: Store<List<RecipeByNutrients>, String>


    init {
        searchRecipesStore = StoreBuilder.key<String, ResponseResult>()
            .fetcher { key ->
                foodieService.searchRecipes(
                    key.split(delimiter)[0],
                    key.split(delimiter)[1],
                    key.split(delimiter)[2],
                    key.split(delimiter)[3],
                    key.split(delimiter)[4],
                    key.split(delimiter)[5].toInt(),
                    key.split(delimiter)[6].toInt()
                )
            }.open()

        getRandomRecipe = StoreBuilder.key<Int, ResponseRecipe>()
            .fetcher { key -> foodieService.getRandomRecipes(key) }
            .open()

        getSimilarRecipes = StoreBuilder.key<Int, Result>()
            .fetcher { key -> foodieService.getSimilarRecipes(key) }
            .open()

        getRecipeById = StoreBuilder.key<Int, Recipe>()
            .fetcher { key -> foodieService.getRecipeInformationById(key) }
            .open()

        getRecipesByNutrients = StoreBuilder.key<String, List<RecipeByNutrients>>()
            .fetcher { key ->
                foodieService.getRecipesByNutrients(
                    key.split(delimiter)[0].toInt(),
                    key.split(delimiter)[1].toInt(),
                    key.split(delimiter)[2].toInt(),
                    key.split(delimiter)[3].toInt(),
                    key.split(delimiter)[4].toInt(),
                    key.split(delimiter)[5].toInt(),
                    key.split(delimiter)[6].toInt(),
                    key.split(delimiter)[7].toInt()
                )
            }.open()
    }

    fun searchRecipesFromStore(key: String): Single<ResponseResult> {
        return searchRecipesStore.get(key)
    }

    fun getRandomRecipesFromStore(key: Int): Single<ResponseRecipe> {
        return getRandomRecipe.get(key)
    }

    fun getSimilarRecipesFromStore(key: Int): Single<Result> {
        return getSimilarRecipes.get(key)
    }

    fun getRecipeInformationByIdFromStore(key: Int): Single<Recipe> {
        return getRecipeById.get(key)
    }

    fun getRecipesByNutrients(key: String): Single<List<RecipeByNutrients>> {
        return getRecipesByNutrients.get(key)
    }
}