package com.example.diegopizzo.foodietest.ui.searchview

import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.business.network.datamodel.Result
import com.example.diegopizzo.foodietest.business.network.service.FoodieService
import com.example.diegopizzo.foodietest.ui.utils.glideOptions
import com.example.diegopizzo.foodietest.ui.utils.titleTruncate
import java.util.*

class SearchViewAdapter(private val context: Context, private val listener: OnSearchItemClickListener) :
    RecyclerView.Adapter<SearchViewAdapter.ViewHolder>(),
    Filterable {

    private val recipeList: MutableList<Result> = mutableListOf()
    private val databaseHistory = SearchHistoryDatabase(context)
    private val historyList = databaseHistory.getAllItems()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context: Context = parent.context
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view: View = inflater.inflate(R.layout.search_view_suggestions_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return recipeList.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    fun addItemsIntoTheList(recipeListToAdd: List<Result>) {
        recipeList.clear()
        recipeList.addAll(recipeListToAdd)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val recipe = recipeList[position]
        if (recipe.image != null) {
            showRecipeSuggestionLayout(holder)
            holder.titleTextView.text = titleTruncate(recipe.title)
            Glide.with(context)
                .load(FoodieService.BASE_URI_IMAGES + recipe.image)
                .apply(glideOptions(context))
                .into(holder.recipeImageView)
        } else {
            showRecipeHistoryLayout(holder)
            holder.historyTitle.text = titleTruncate(recipe.title)
        }
        holder.bind(recipe, listener)
    }

    private fun showRecipeHistoryLayout(holder: ViewHolder) {
        holder.historyCard.visibility = VISIBLE
        holder.recipeCard.visibility = GONE
    }

    private fun showRecipeSuggestionLayout(holder: ViewHolder) {
        holder.historyCard.visibility = GONE
        holder.recipeCard.visibility = VISIBLE
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val query = constraint.toString().toLowerCase(Locale.getDefault())
                val results = FilterResults()
                if (TextUtils.isEmpty(query)) {
                    results.count = historyList.size
                } else {
                    results.count = recipeList.size
                }
                return results
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                val query = constraint.toString().toLowerCase(Locale.getDefault())
                if (TextUtils.isEmpty(query)) {
                    addItemsIntoTheList(historyList)
                }
            }
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val recipeCard: CardView = itemView.findViewById(R.id.recipeItemCard)
        val historyCard: CardView = itemView.findViewById(R.id.historyCard)
        val recipeImageView: ImageView = itemView.findViewById(R.id.imageItemRecipe)
        val titleTextView: TextView = itemView.findViewById(R.id.titleItemRecipe)
        val historyTitle: TextView = itemView.findViewById(R.id.historyTitle)

        fun bind(recipe: Result, listener: OnSearchItemClickListener) {
            itemView.setOnClickListener {
                listener.onSearchItemClick(recipe)
            }
        }
    }

    interface OnSearchItemClickListener {
        fun onSearchItemClick(recipe: Result)
    }
}