package com.example.diegopizzo.foodietest.ui.customview

import android.content.Context
import android.util.Log
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.business.creator.Creators
import com.example.diegopizzo.foodietest.business.db.repository.RecipeRepository
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import com.example.diegopizzo.foodietest.ui.utils.glideOptions
import com.google.android.material.snackbar.Snackbar
import com.mindorks.placeholderview.annotations.Click
import com.mindorks.placeholderview.annotations.Layout
import com.mindorks.placeholderview.annotations.Resolve
import com.mindorks.placeholderview.annotations.View
import com.mindorks.placeholderview.annotations.swipe.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.koin.standalone.KoinComponent
import org.koin.standalone.inject


//https://janishar.com/PlaceHolderView/docs/kotlin.html
//https://janishar.com/PlaceHolderView/docs/sphv-view-definition.html
@Layout(R.layout.card_view_layout)
class FoodieCard(
    private val context: Context,
    private val recipe: Recipe,
    private val isLastCard: Boolean,
    private val onFragmentInteractorListener: OnFragmentInteractorListener? = null
) : KoinComponent {

    private val recipeRepository: RecipeRepository by inject()
    private val creators: Creators by inject()

    @SwipeView
    lateinit var swipeView: android.view.View

    @View(R.id.titleTextView)
    lateinit var titleTextView: TextView

    @View(R.id.foodieImageView)
    lateinit var foodieImageView: ImageView

    private lateinit var disposable: Disposable


    @Resolve
    fun onResolved() {
        if (!isLastCard) {
            setImageToImageView(recipe.image)
        } else {
            setImageToImageView(R.drawable.last_card_foodie_test)
        }
        titleTextView.text = recipe.title
    }

    @SwipeIn
    fun onSwipeIn() {
        saveRecipeOnDb(true)
        Log.d("EVENT", "onSwipedIn")
    }

    private fun saveRecipeOnDb(boolean: Boolean) {
        val addRecipeCompletable =
            recipeRepository.insertAll(creators.fromRecipeDataModelToRecipeDbEntity(recipe, boolean))
        disposable = addRecipeCompletable
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    Log.i("DB", "Saved")
                },
                { throwable ->
                    Log.i("ErrorDB", throwable.message)
                })
    }

    @SwipeOut
    fun onSwipedOut() {
        saveRecipeOnDb(false)
        Log.d("EVENT", "onSwipedOut")
    }

    //It is called when the item view is not swiped either in or out. It is restored into the stack.
    @SwipeCancelState
    fun onSwipeCancelState() {
        Log.d("EVENT", "onSwipeCancelState")
    }

    //It is called repeatedly as the the item view moves in the left/top direction.
    @SwipeInState
    fun onSwipeInState() {
        Log.d("EVENT", "onSwipeInState")
    }

    //It is called repeatedly as the the item view moves in the right/top direction.
    @SwipeOutState
    fun onSwipeOutState() {
        Log.d("EVENT", "onSwipeOutState")
    }

    //When a card takes the head of the stack then @SwipeHead is called.
    //It can be used to do layout changes or animation when the card takes the top position
    @SwipeHead
    fun onSwipeHead() {
        if (isLastCard) {
            onFragmentInteractorListener?.removeClickListener()
        }
        Log.d("EVENT", "onSwipeHead")
    }

    @Click(R.id.foodieImageView)
    fun onClick() {
        if (isLastCard) {
            Snackbar.make(swipeView, "Last Card", Snackbar.LENGTH_LONG).show()
        }
        Log.d("EVENT", "imageView click")
    }

    private fun setImageToImageView(image: String?) {
        Glide.with(context).load(image).apply(glideOptions(context))
            .into(foodieImageView)
    }

    private fun setImageToImageView(image: Int) {
        Glide.with(context).load(image).apply(glideOptions(context))
            .into(foodieImageView)
    }


    interface OnFragmentInteractorListener {
        fun removeClickListener()
    }
}