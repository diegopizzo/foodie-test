package com.example.diegopizzo.foodietest.ui.utils

import android.content.Context
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import com.bumptech.glide.request.RequestOptions
import com.example.diegopizzo.foodietest.R

fun glideOptions(context: Context): RequestOptions {
    return RequestOptions()
        .centerCrop().placeholder(setProgressBar(context))
        .error(R.drawable.idontknow)
}


fun setProgressBar(context: Context): CircularProgressDrawable {
    val circularProgressDrawable = CircularProgressDrawable(context)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.start()
    return circularProgressDrawable
}

fun titleTruncate(title: String): String = if (title.length > TITLE_MAX_LENGTH)
    title.take(TITLE_MAX_LENGTH).plus("...") else title

fun instructionsTruncate(instructions: String): String = if (instructions.length > INSTRUCTIONS_MAX_LENGTH)
    instructions.take(INSTRUCTIONS_MAX_LENGTH).plus("...show more") else instructions

fun removeHtmlTags(html: String): String {
    html.replace("<(.*?)>".toRegex(), " ")
    html.replace("<(.*?)\n".toRegex(), " ")
    html.replaceFirst("(.*?)>".toRegex(), " ")
    html.replace("&nbsp;".toRegex(), " ")
    html.replace("&amp;".toRegex(), " ")
    html.replace("\n".toRegex(), " ")
    return html
}