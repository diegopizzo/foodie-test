package com.example.diegopizzo.foodietest.ui.recipedetailsactivity

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import com.example.diegopizzo.foodietest.business.network.interactor.FoodieInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class RecipeDetailsViewModel(private val interactor: FoodieInteractor) : ViewModel() {

    private lateinit var disposable: Disposable
    val recipeDetailsMutableLiveData = MutableLiveData<Recipe>()
    val progressBarVisibility: MutableLiveData<Int> = MutableLiveData()
    val status: MutableLiveData<String> = MutableLiveData()


    fun getRecipeDetails(id: Int) {
        val recipeDetails = interactor.getRecipeInformationById(id)
        disposable = recipeDetails
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onStart() }
            .doFinally { onEnd() }
            .subscribe({ recipe ->
                onSuccess(recipe)
                setErrorMessage(null)
            },
                { throwable ->
                    Log.i("Error", throwable?.message)
                    setErrorMessage(ERROR_MESSAGE)
                })
    }

    private fun onStart() {
        progressBarVisibility.value = View.VISIBLE
        status.value = null
    }

    private fun onEnd() {
        progressBarVisibility.value = View.GONE
    }

    private fun onSuccess(recipeDetails: Recipe) {
        recipeDetailsMutableLiveData.value = recipeDetails
    }

    private fun setErrorMessage(message: String?) {
        status.value = message
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }

    companion object {
        private const val ERROR_MESSAGE = "Recipe does't exists"
    }
}