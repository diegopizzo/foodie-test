package com.example.diegopizzo.foodietest.config.koin

import androidx.room.Room
import com.example.diegopizzo.foodietest.business.db.config.AppDatabase
import com.example.diegopizzo.foodietest.business.db.datasource.RecipeDataSourceLikedFactory
import com.example.diegopizzo.foodietest.business.db.datasource.RecipeDataSourceNotLikedFactory
import com.example.diegopizzo.foodietest.business.db.datasource.RecipeItemKeyedDataSourceLiked
import com.example.diegopizzo.foodietest.business.db.datasource.RecipeItemKeyedDataSourceNotLiked
import com.example.diegopizzo.foodietest.business.db.repository.RecipeRepository
import com.example.diegopizzo.foodietest.business.db.repository.impl.RecipeRepositoryImpl
import org.koin.android.ext.koin.androidApplication
import org.koin.dsl.module.module

val dbModule = module {

    //DB
    single {
        Room.databaseBuilder(androidApplication(), AppDatabase::class.java, "foodorama").allowMainThreadQueries()
            .build()
    }

    //Dao
    single { get<AppDatabase>().recipeDao() }

    //Repository
    single { RecipeRepositoryImpl(get()) as RecipeRepository }

    //Custom datasource
    single { RecipeItemKeyedDataSourceLiked() }
    single { RecipeItemKeyedDataSourceNotLiked() }
    single { RecipeDataSourceLikedFactory(get()) }
    single { RecipeDataSourceNotLikedFactory(get()) }
}