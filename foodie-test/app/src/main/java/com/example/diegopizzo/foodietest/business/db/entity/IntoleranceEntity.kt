package com.example.diegopizzo.foodietest.business.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey
import com.example.diegopizzo.foodietest.ui.utils.Intolerances

@Entity(tableName = "intolerance", indices = [Index(value = ["name"], unique = true)])
data class IntoleranceEntity(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "name") var name: Intolerances
)