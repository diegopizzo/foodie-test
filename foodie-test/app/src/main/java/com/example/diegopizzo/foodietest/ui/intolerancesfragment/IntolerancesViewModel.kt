package com.example.diegopizzo.foodietest.ui.intolerancesfragment

import androidx.lifecycle.ViewModel
import com.example.diegopizzo.foodietest.business.db.repository.RecipeRepository
import com.example.diegopizzo.foodietest.ui.utils.Intolerances

class IntolerancesViewModel(private val repository: RecipeRepository) : ViewModel() {

    fun addIntolerance(intolerances: Intolerances) {
        repository.insertByIntoleranceName(intolerances)
    }

    fun removeIntolerance(intolerances: Intolerances) {
        repository.deleteIntoleranceByName(intolerances)
    }

    fun loadIntolerances(): List<String> {
        return repository.loadIntolerances().map { intoleranceEntity -> intoleranceEntity.name.intolerance }
    }
}