package com.example.diegopizzo.foodietest.ui.utils

import androidx.paging.PagedList

enum class Intolerances(val intolerance: String) {
    DAIRY("Dairy"), EGG("Egg"), GLUTEN("Gluten"), PEANUT("Peanut"),
    SESAME("Sesame"), SEAFOOD("Seafood"), SHELLFISH("Shellfish"), SOY("Soy"),
    SULFITE("Sulfite"), TREE_NUT("Tree nut"), WHEAT("Wheat");

    companion object {
        fun getValueFromIntolerance(intolerances: Intolerances): String {
            return intolerances.intolerance
        }

        fun getIntoleranceFromValue(string: String): Intolerances {
            Intolerances.values().map { t ->
                if (t.intolerance == string) return t
            }
            throw EnumConstantNotPresentException(Intolerances::class.java, string)
        }
    }
}

enum class Nutrients(val nutrientName: String, var defaultValue: Int, val maxValue: Int, val minValue: Int) {
    CALORIES("Calories", 200, 255, 0), CARBS("Carbs", 70, 150, 0),
    PROTEIN("Protein", 40, 150, 0), FAT("Fat", 20, 80, 0)
}

const val RECIPE_INSTRUCTIONS_NO_VALUE = "No instructions:"
const val RECIPE_INSTRUCTIONS = "Recipe instructions:"
const val TITLE_MAX_LENGTH = 20
const val INSTRUCTIONS_MAX_LENGTH = 60
private const val PAGE_SIZE = 5
private const val INITIAL_LOAD_SIZE = 5
val pagedListConfig = PagedList.Config.Builder()
    .setEnablePlaceholders(false)
    .setInitialLoadSizeHint(INITIAL_LOAD_SIZE)
    .setPageSize(PAGE_SIZE)
    .build()