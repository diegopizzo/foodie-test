package com.example.diegopizzo.foodietest.business.db.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import io.reactivex.disposables.CompositeDisposable

class RecipeItemKeyedDataSourceNotLiked : RecipeItemKeyedDataSourceBase() {

    private val disposable: CompositeDisposable = CompositeDisposable()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Recipe>) {
        val items = recipeRepository.loadAllRecipesNotLiked(requestedLoadSize = params.requestedLoadSize)
        disposable.add(createRecipesSingle(items).subscribe({ recipes -> callback.onResult(recipes) }, {}))
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Recipe>) {
        val items =
            recipeRepository.loadAllRecipesNotLikedAfter(key = params.key, requestedLoadSize = params.requestedLoadSize)
        disposable.add(createRecipesSingle(items).subscribe({ recipes -> callback.onResult(recipes) }, {}))
    }

    override fun invalidate() {
        super.invalidate()
        disposable.clear()
    }
}

class RecipeDataSourceNotLikedFactory(private val dataSource: RecipeItemKeyedDataSourceNotLiked) :
    DataSource.Factory<Int, Recipe>() {

    private val recipesLiveData = MutableLiveData<RecipeItemKeyedDataSourceNotLiked>()

    override fun create(): DataSource<Int, Recipe> {
        recipesLiveData.postValue(dataSource)
        return dataSource
    }
}