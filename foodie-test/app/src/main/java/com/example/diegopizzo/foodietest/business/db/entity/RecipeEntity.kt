package com.example.diegopizzo.foodietest.business.db.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "recipe", indices = [Index(value = ["id_recipe"], unique = true)])
data class RecipeEntity(
    @PrimaryKey(autoGenerate = true) var id: Int,
    @ColumnInfo(name = "id_recipe") var idRecipe: Int,
    @ColumnInfo(name = "title") var title: String?,
    @ColumnInfo(name = "is_liked") var isLiked: Boolean?
)