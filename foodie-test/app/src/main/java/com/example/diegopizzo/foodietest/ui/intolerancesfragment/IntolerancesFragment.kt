package com.example.diegopizzo.foodietest.ui.intolerancesfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.databinding.IntolerancesFragmentLayoutBinding
import com.example.diegopizzo.foodietest.ui.utils.Intolerances
import org.koin.android.viewmodel.ext.android.viewModel

class IntolerancesFragment : Fragment() {

    private val viewModel by viewModel<IntolerancesViewModel>()
    private lateinit var binding: IntolerancesFragmentLayoutBinding

    companion object {
        const val TAG_INTOLERANCES_FRAGMENT = "intolerancesFragment"

        fun newInstance(bundle: Bundle?): IntolerancesFragment {
            val intolerancesFragment = IntolerancesFragment()
            if (bundle != null) {
                intolerancesFragment.arguments = bundle
            }
            return intolerancesFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.intolerances_fragment_layout, container, false)
        binding.viewModel = viewModel
        setupRecyclerView()
        return binding.root
    }

    private fun setupRecyclerView() {
        val adapter = CheckboxListAdapter(object : CheckboxListAdapter.OnFragmentInteractorListener {
            override fun addIntolerance(intolerances: Intolerances) {
                viewModel.addIntolerance(intolerances)
            }

            override fun removeIntolerance(intolerances: Intolerances) {
                viewModel.removeIntolerance(intolerances)
            }
        })
        adapter.intolerancesFromDb = viewModel.loadIntolerances()
        binding.intolerancesRecyclerView.layoutManager = LinearLayoutManager(context!!, RecyclerView.VERTICAL, false)
        binding.intolerancesRecyclerView.adapter = adapter
    }
}