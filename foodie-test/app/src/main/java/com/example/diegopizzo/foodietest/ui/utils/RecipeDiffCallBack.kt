package com.example.diegopizzo.foodietest.ui.utils

import androidx.recyclerview.widget.DiffUtil
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe

class RecipeDiffCallBack : DiffUtil.ItemCallback<Recipe>() {

    override fun areContentsTheSame(oldItem: Recipe, newItem: Recipe): Boolean {
        return oldItem == newItem
    }

    override fun areItemsTheSame(oldItem: Recipe, newItem: Recipe): Boolean {
        return oldItem.id == newItem.id
    }
}