package com.example.diegopizzo.foodietest.ui.recipesbynutrientsfragment.recipesbynutrientsvalues

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.diegopizzo.foodietest.business.network.datamodel.RecipeByNutrients
import com.example.diegopizzo.foodietest.business.network.interactor.FoodieInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class RecipesByNutrientsValuesViewModel(private val interactor: FoodieInteractor) : ViewModel() {

    private lateinit var disposable: Disposable
    val recipesByNutrientsMutableLiveData: MutableLiveData<List<RecipeByNutrients>> = MutableLiveData()
    val progressBarRecipesByNutrientsVisibility: MutableLiveData<Int> = MutableLiveData()

    fun getRecipesByNutrients(values: IntArray) {
        val single =
            interactor.getRecipesByNutrients(values[0], values[1], values[2], values[3])
        disposable = single.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onStart() }
            .doFinally { onEnd() }
            .subscribe({ recipesByNutrientsList -> onSuccess(recipesByNutrientsList) },
                { t: Throwable? -> onError(t) })
    }

    private fun onStart() {
        progressBarRecipesByNutrientsVisibility.value = View.VISIBLE
    }

    private fun onEnd() {
        progressBarRecipesByNutrientsVisibility.value = View.GONE
    }

    private fun onSuccess(recipesByNutrientsList: List<RecipeByNutrients>) {
        recipesByNutrientsMutableLiveData.value = recipesByNutrientsList
    }

    private fun onError(throwable: Throwable?) {
        Log.i("Error", throwable?.message)
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}