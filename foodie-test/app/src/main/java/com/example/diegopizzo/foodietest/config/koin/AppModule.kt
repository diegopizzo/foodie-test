package com.example.diegopizzo.foodietest.config.koin

import com.example.diegopizzo.foodietest.ui.foodietestfragment.FoodieTestViewModel
import com.example.diegopizzo.foodietest.ui.homefragment.HomeViewModel
import com.example.diegopizzo.foodietest.ui.intolerancesfragment.IntolerancesViewModel
import com.example.diegopizzo.foodietest.ui.recipedetailsactivity.RecipeDetailsViewModel
import com.example.diegopizzo.foodietest.ui.recipesbynutrientsfragment.recipesbynutrientsvalues.RecipesByNutrientsValuesViewModel
import com.example.diegopizzo.foodietest.ui.recipeslistfragment.likedrecipesfragment.LikedRecipesViewModel
import com.example.diegopizzo.foodietest.ui.recipeslistfragment.notlikedrecipesfragment.NotLikedRecipesViewModel
import com.example.diegopizzo.foodietest.ui.searchview.SearchViewModel
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module


val appModule = module {

    viewModel { FoodieTestViewModel(get()) }
    viewModel { HomeViewModel(get()) }
    viewModel { LikedRecipesViewModel(get()) }
    viewModel { NotLikedRecipesViewModel(get()) }
    viewModel { IntolerancesViewModel(get()) }
    viewModel { SearchViewModel(get(), get()) }
    viewModel { RecipeDetailsViewModel(get()) }
    viewModel { RecipesByNutrientsValuesViewModel(get()) }
}