package com.example.diegopizzo.foodietest.ui.homefragment

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import com.example.diegopizzo.foodietest.business.network.interactor.FoodieInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlin.random.Random

class HomeViewModel(private val interactor: FoodieInteractor) : ViewModel() {


    companion object {
        const val RANDOM_RECIPES_NUMBER: Int = 10
        private const val RANDOM_MAX_VALUE = 999999
    }

    private val disposable = CompositeDisposable()
    val progressBarPopularVisibility: MutableLiveData<Int> = MutableLiveData()
    val progressBarRecommendedVisibility: MutableLiveData<Int> = MutableLiveData()
    val progressBarRecipeOfDayVisibility: MutableLiveData<Int> = MutableLiveData()
    val recipesPopularListMutableLiveData: MutableLiveData<List<Recipe>> = MutableLiveData()
    val recipesRecommendedListMutableLiveData: MutableLiveData<List<Recipe>> = MutableLiveData()
    val recipeMutableLiveData: MutableLiveData<Recipe> = MutableLiveData()


    fun getPopularRecipes() {
        val randomRecipes = interactor.getRandomRecipes(Random.nextInt(RANDOM_RECIPES_NUMBER) + 2)
        disposable.add(randomRecipes.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onStartPopular() }
            .doFinally { onEndPopular() }
            .subscribe(
                { responseRecipe ->
                    if (responseRecipe != null && !responseRecipe.recipes.isEmpty()) {
                        onSuccessPopularRecipes(responseRecipe.recipes)
                    }
                },
                { throwable ->
                    Log.i("Error", throwable?.message)
                })
        )
    }

    fun getRecommendedRecipes() {
        val randomRecipes = interactor.getRandomRecipes(RANDOM_RECIPES_NUMBER)
        disposable.add(randomRecipes.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onStartRecommended() }
            .doFinally { onEndRecommended() }
            .subscribe(
                { responseRecipe ->
                    if (responseRecipe != null && !responseRecipe.recipes.isEmpty()) {
                        onSuccessRecommendedRecipes(responseRecipe.recipes)
                    }
                },
                { throwable ->
                    Log.i("Error", throwable?.message)
                })
        )
    }

    fun getRandomRecipe() {
        val randomId: Int = Random.nextInt(RANDOM_MAX_VALUE)
        val randomRecipe = interactor.getRecipeInformationById(randomId)
        disposable.add(randomRecipe
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onStartRecipe() }
            .doFinally { onEndRecipe() }
            .subscribe(
                { recipe -> onSuccessRandomRecipe(recipe) },
                { throwable ->
                    Log.i("Error", throwable?.message)
                    getRandomRecipe()
                })
        )
    }

    private fun onStartPopular() {
        progressBarPopularVisibility.value = View.VISIBLE
    }

    private fun onEndPopular() {
        progressBarPopularVisibility.value = View.GONE
    }

    private fun onStartRecommended() {
        progressBarRecommendedVisibility.value = View.VISIBLE
    }

    private fun onEndRecommended() {
        progressBarRecommendedVisibility.value = View.GONE
    }

    private fun onStartRecipe() {
        progressBarRecipeOfDayVisibility.value = View.VISIBLE
    }

    private fun onEndRecipe() {
        progressBarRecipeOfDayVisibility.value = View.GONE
    }

    private fun onSuccessPopularRecipes(recipes: List<Recipe>) {
        recipesPopularListMutableLiveData.value = recipes
    }

    private fun onSuccessRecommendedRecipes(recipes: List<Recipe>) {
        recipesRecommendedListMutableLiveData.value = recipes
    }

    private fun onSuccessRandomRecipe(recipe: Recipe) {
        recipeMutableLiveData.value = recipe
    }

    override fun onCleared() {
        super.onCleared()
        disposable.clear()
    }
}