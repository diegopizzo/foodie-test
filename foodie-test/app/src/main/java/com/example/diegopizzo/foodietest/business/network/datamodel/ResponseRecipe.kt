package com.example.diegopizzo.foodietest.business.network.datamodel

import com.google.gson.annotations.Expose

data class ResponseRecipe(@Expose val recipes: List<Recipe>)