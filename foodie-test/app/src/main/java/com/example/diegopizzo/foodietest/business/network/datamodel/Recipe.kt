package com.example.diegopizzo.foodietest.business.network.datamodel

import com.google.gson.annotations.Expose

data class Recipe(
    @Expose val extendedIngredients: List<ExtendedIngredient>?,
    @Expose val id: Int,
    @Expose val title: String,
    @Expose val image: String?,
    @Expose val instructions: String?
)