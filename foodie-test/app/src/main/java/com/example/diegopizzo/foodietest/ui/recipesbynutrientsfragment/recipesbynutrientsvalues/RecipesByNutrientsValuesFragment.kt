package com.example.diegopizzo.foodietest.ui.recipesbynutrientsfragment.recipesbynutrientsvalues

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.databinding.RecipesByNutrientsValuesLayoutBinding
import com.example.diegopizzo.foodietest.ui.recipedetailsactivity.RecipeDetailsActivity
import com.example.diegopizzo.foodietest.ui.recipesbynutrientsfragment.recipesbynutrientssetter.RecipesByNutrientsSetterFragment.Companion.NUTRIENTS_INT_ARRAY_KEY
import org.koin.android.viewmodel.ext.android.viewModel

class RecipesByNutrientsValuesFragment : Fragment() {

    private lateinit var binding: RecipesByNutrientsValuesLayoutBinding
    private val viewModel: RecipesByNutrientsValuesViewModel by viewModel()
    private lateinit var adapter: RecipesByNutrientsValuesAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.recipes_by_nutrients_values_layout, container, false)
        binding.viewModel = viewModel

        val recipesByNutrientsSetterValues = arguments?.getIntArray(NUTRIENTS_INT_ARRAY_KEY)

        viewModel.getRecipesByNutrients(recipesByNutrientsSetterValues!!)

        viewModel.recipesByNutrientsMutableLiveData.observe(
            this,
            Observer { recipesByNutrientsList -> adapter.addListIntoAdapter(recipesByNutrientsList) })

        setRecyclerView()
        return binding.root
    }

    private fun setRecyclerView() {
        binding.nutrientsValuesRecyclerView.layoutManager = LinearLayoutManager(context)
        adapter = RecipesByNutrientsValuesAdapter(object : RecipesByNutrientsValuesAdapter.OnItemClickListener {
            override fun onItemClick(id: Int) {
                startRecipeDetailsActivity(id)
            }

        })
        binding.nutrientsValuesRecyclerView.adapter = adapter
    }

    private fun startRecipeDetailsActivity(id: Int) {
        val intent =
            Intent(context, RecipeDetailsActivity::class.java).putExtra(RecipeDetailsActivity.RECIPE_ID_BUNDLE_KEY, id)
        startActivity(intent)
    }

    companion object {
        const val TAG_RECIPES_BY_NUTRIENTS_VALUES_FRAGMENT = "recipesByNutrientsValuesFragment"

        fun newInstance(bundle: Bundle?): RecipesByNutrientsValuesFragment {
            val recipesByNutrientsValuesFragment =
                RecipesByNutrientsValuesFragment()
            if (bundle != null) {
                recipesByNutrientsValuesFragment.arguments = bundle
            }
            return recipesByNutrientsValuesFragment
        }
    }
}