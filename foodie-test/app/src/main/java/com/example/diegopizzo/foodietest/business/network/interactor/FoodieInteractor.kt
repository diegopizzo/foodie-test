package com.example.diegopizzo.foodietest.business.network.interactor

import com.example.diegopizzo.foodietest.business.network.cache.FoodieStore
import com.example.diegopizzo.foodietest.business.network.datamodel.*
import com.example.diegopizzo.foodietest.testing.OpenForTesting
import io.reactivex.Single

@OpenForTesting
class FoodieInteractor(private val foodieStore: FoodieStore) {

    fun searchRecipes(
        query: String, cuisine: String?, diet: String?, excludeIngredients: String?,
        intolerances: String?, number: Int, offset: Int
    ): Single<ResponseResult> {
        return foodieStore.searchRecipesFromStore("$query;$cuisine;$diet;$excludeIngredients;$intolerances;$number;$offset")
    }

    fun getRandomRecipes(number: Int): Single<ResponseRecipe> {
        return foodieStore.getRandomRecipesFromStore(number)
    }

    fun getSimilarRecipes(id: Int): Single<Result> {
        return foodieStore.getSimilarRecipesFromStore(id)
    }

    fun getRecipeInformationById(id: Int): Single<Recipe> {
        return foodieStore.getRecipeInformationByIdFromStore(id)
    }

    fun getRecipesByNutrients(
        maxCalories: Int, maxCarbs: Int, maxProtein: Int, maxFat: Int
    ): Single<List<RecipeByNutrients>> {
        return foodieStore.getRecipesByNutrients("0;0;0;0;$maxCalories;$maxCarbs;$maxProtein;$maxFat")
    }
}