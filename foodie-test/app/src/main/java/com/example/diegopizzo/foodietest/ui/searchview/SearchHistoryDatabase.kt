package com.example.diegopizzo.foodietest.ui.searchview

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.diegopizzo.foodietest.business.network.datamodel.Result
import com.example.diegopizzo.foodietest.ui.searchview.HistoryDatabaseHelper.Companion.SEARCH_HISTORY_COLUMN_ID
import com.example.diegopizzo.foodietest.ui.searchview.HistoryDatabaseHelper.Companion.SEARCH_HISTORY_COLUMN_TITLE
import com.example.diegopizzo.foodietest.ui.searchview.HistoryDatabaseHelper.Companion.SEARCH_HISTORY_TABLE
import java.lang.ref.WeakReference

class SearchHistoryDatabase(private val context: Context) {

    private val weakReference: WeakReference<Context> = WeakReference(context)
    private lateinit var databaseHelper: HistoryDatabaseHelper
    private lateinit var database: SQLiteDatabase

    private fun open() {
        databaseHelper = HistoryDatabaseHelper(weakReference.get()!!)
        database = databaseHelper.writableDatabase
    }

    private fun close() {
        databaseHelper.close()
    }

    private fun isIdAlreadyPresent(id: Int): Boolean {
        open()

        val query =
            "SELECT * FROM $SEARCH_HISTORY_TABLE WHERE $SEARCH_HISTORY_COLUMN_ID =?"
        val cursor = database.rawQuery(query, arrayOf(id.toString()))

        var hasObject = false

        if (cursor.moveToFirst()) {
            hasObject = true
        }

        cursor.close()
        close()
        return hasObject
    }

    fun addItemRecipe(recipe: Result) {
        val contentValues = ContentValues()
        contentValues.put(SEARCH_HISTORY_COLUMN_ID, recipe.id)
        contentValues.put(SEARCH_HISTORY_COLUMN_TITLE, recipe.title)
        if (!isIdAlreadyPresent(recipe.id)) {
            open()
            database.insert(SEARCH_HISTORY_TABLE, null, contentValues)
            close()
        } else {
            open()
            database.update(
                SEARCH_HISTORY_TABLE,
                contentValues,
                "$SEARCH_HISTORY_COLUMN_ID = ?",
                arrayOf(recipe.id.toString())
            )
            close()
        }

    }

    fun getAllItems(): List<Result> {
        val listItems = arrayListOf<Result>()
        val query = "SELECT * FROM $SEARCH_HISTORY_TABLE"
        open()
        val cursor = database.rawQuery(query, null)
        if (cursor.moveToFirst()) {
            do {
                listItems.add(Result(cursor.getInt(0), cursor.getString(1), null))
            } while (cursor.moveToNext())
        }
        cursor.close()
        close()
        return listItems
    }

    fun clearDatabase() {
        open()
        database.delete(HistoryDatabaseHelper.SEARCH_HISTORY_TABLE, null, null)
        close()
    }
}