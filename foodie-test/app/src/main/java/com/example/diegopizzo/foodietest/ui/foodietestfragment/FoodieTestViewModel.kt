package com.example.diegopizzo.foodietest.ui.foodietestfragment

import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import com.example.diegopizzo.foodietest.business.network.interactor.FoodieInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class FoodieTestViewModel(private val interactor: FoodieInteractor) : ViewModel() {

    companion object {
        private const val RANDOM_RECIPES_NUMBER: Int = 10
    }

    private lateinit var disposable: Disposable
    val progressBarVisibility: MutableLiveData<Int> = MutableLiveData()
    val recipesListMutableLiveData: MutableLiveData<List<Recipe>> = MutableLiveData()

    fun getRandomRecipes() {
        val randomRecipes = interactor.getRandomRecipes(RANDOM_RECIPES_NUMBER)
        disposable = randomRecipes.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onStart() }
            .doFinally { onEnd() }
            .subscribe(
                { responseRecipe ->
                    if (responseRecipe != null && !responseRecipe.recipes.isEmpty()) {
                        onSuccess(responseRecipe.recipes)
                    }
                },
                { throwable ->
                    Log.i("Error", throwable?.message)
                })
    }

    private fun onStart() {
        progressBarVisibility.value = View.VISIBLE
    }

    private fun onEnd() {
        progressBarVisibility.value = View.GONE
    }

    private fun onSuccess(recipes: List<Recipe>) {
        recipesListMutableLiveData.value = recipes
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}