package com.example.diegopizzo.foodietest.ui.customview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.cardview.widget.CardView
import com.example.diegopizzo.foodietest.R

class ExtendedRecipeItemView(context: Context, attrs: AttributeSet?) : CardView(context, attrs) {

    init {
        View.inflate(context, R.layout.recipe_item_extended_view_layout, this)
    }
}