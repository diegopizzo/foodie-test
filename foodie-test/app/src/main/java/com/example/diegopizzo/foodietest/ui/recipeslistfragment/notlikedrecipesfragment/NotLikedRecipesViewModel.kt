package com.example.diegopizzo.foodietest.ui.recipeslistfragment.notlikedrecipesfragment

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import com.example.diegopizzo.foodietest.business.db.datasource.RecipeDataSourceNotLikedFactory
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import com.example.diegopizzo.foodietest.ui.utils.pagedListConfig
import io.reactivex.disposables.Disposable

class NotLikedRecipesViewModel(dataSourceFactory: RecipeDataSourceNotLikedFactory) : ViewModel() {


    private lateinit var disposable: Disposable
    val progressBarVisibility: MutableLiveData<Int> = MutableLiveData()
    val noLikedRecipesImageViewVisibility: MutableLiveData<Int> = MutableLiveData()

    val recipeEntitiesLiveData =
        LivePagedListBuilder<Int, Recipe>(dataSourceFactory, pagedListConfig).build()


    private fun onStart() {
        progressBarVisibility.value = View.VISIBLE
    }

    private fun onEnd() {
        progressBarVisibility.value = View.GONE
    }

    private fun onSuccess() {
        noLikedRecipesImageViewVisibility.value = View.GONE
    }

    private fun onError() {
        noLikedRecipesImageViewVisibility.value = View.VISIBLE
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}