package com.example.diegopizzo.foodietest.ui.homefragment

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import com.example.diegopizzo.foodietest.ui.utils.glideOptions
import com.example.diegopizzo.foodietest.ui.utils.titleTruncate

class HomeRecyclerViewAdapter(private val context: Context, private val listener: OnItemClickListener) :
    RecyclerView.Adapter<HomeRecyclerViewAdapter.ViewHolder>() {

    private val recipeList: MutableList<Recipe> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        val context: Context = parent.context
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view: View = inflater.inflate(R.layout.home_recipe_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return recipeList.size
    }

    fun addItemsIntoTheList(recipeListToAdd: List<Recipe>) {
        recipeList.clear()
        recipeList.addAll(recipeListToAdd)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val recipe = recipeList[position]
        val textView = holder.titleTextView
        textView.text = titleTruncate(recipe.title)

        Glide.with(context).load(recipe.image).apply(glideOptions(context))
            .into(holder.recipeImageView)
        holder.setOnClickListener(recipe.id)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val recipeImageView: ImageView = itemView.findViewById(R.id.imageItemRecipe)
        val titleTextView: TextView = itemView.findViewById(R.id.titleItemRecipe)

        fun setOnClickListener(id: Int) {
            itemView.setOnClickListener {
                listener.onItemClick(id)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(id: Int)
    }
}