package com.example.diegopizzo.foodietest.ui.recipeslistfragment.notlikedrecipesfragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.databinding.NotLikedRecipesListLayoutBinding
import com.example.diegopizzo.foodietest.ui.recipedetailsactivity.RecipeDetailsActivity
import com.example.diegopizzo.foodietest.ui.recipeslistfragment.RecipesListAdapter
import org.koin.android.viewmodel.ext.android.viewModel

class NotLikedRecipesFragment : Fragment() {

    private val viewModel by viewModel<NotLikedRecipesViewModel>()
    private lateinit var binding: NotLikedRecipesListLayoutBinding
    private lateinit var recipesListAdapter: RecipesListAdapter

    companion object {
        const val VIEW_PAGER_TITLE = "Not liked recipes"
        const val VIEW_PAGER_POSITION = 1

        fun newInstance(bundle: Bundle?): NotLikedRecipesFragment {
            val notLikedRecipesFragment = NotLikedRecipesFragment()
            if (bundle != null) {
                notLikedRecipesFragment.arguments = bundle
            }
            return notLikedRecipesFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        recipesListAdapter = RecipesListAdapter(context!!, object : RecipesListAdapter.OnItemClickListener {
            override fun onItemClick(id: Int) {
                startRecipeDetailsActivity(id)
            }

        })
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.not_liked_recipes_list_layout, container, false)
        binding.viewModel = viewModel
        setRecyclerView()

        viewModel.recipeEntitiesLiveData.observe(this, Observer { recipesListAdapter.submitList(it) })

        return binding.root
    }

    private fun setRecyclerView() {
        binding.notLikedRecipesRecyclerView.adapter = recipesListAdapter
        binding.notLikedRecipesRecyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
    }

    private fun startRecipeDetailsActivity(id: Int) {
        val intent =
            Intent(context, RecipeDetailsActivity::class.java).putExtra(RecipeDetailsActivity.RECIPE_ID_BUNDLE_KEY, id)
        startActivity(intent)
    }
}