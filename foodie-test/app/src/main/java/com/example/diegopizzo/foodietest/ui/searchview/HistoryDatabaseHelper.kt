package com.example.diegopizzo.foodietest.ui.searchview

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

internal class HistoryDatabaseHelper(context: Context) :
    SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(CREATE_TABLE_SEARCH_HISTORY)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        dropAllTables(db)
        onCreate(db)
    }

    private fun dropAllTables(db: SQLiteDatabase) {
        dropTableIfExists(db)
    }

    private fun dropTableIfExists(db: SQLiteDatabase) {
        db.execSQL("DROP TABLE IF EXISTS $SEARCH_HISTORY_TABLE")
    }

    companion object {

        internal const val SEARCH_HISTORY_TABLE = "search_history"
        internal const val SEARCH_HISTORY_COLUMN_ID = "_id"
        internal const val SEARCH_HISTORY_COLUMN_TITLE = "_title"

        private const val DATABASE_VERSION = 1
        private const val DATABASE_NAME = "searchHistoryDatabase.db"
        private const val CREATE_TABLE_SEARCH_HISTORY = ("CREATE TABLE IF NOT EXISTS "
                + SEARCH_HISTORY_TABLE + " ( "
                + SEARCH_HISTORY_COLUMN_ID + " INTEGER PRIMARY KEY, "
                + SEARCH_HISTORY_COLUMN_TITLE + " TEXT);")
    }
}