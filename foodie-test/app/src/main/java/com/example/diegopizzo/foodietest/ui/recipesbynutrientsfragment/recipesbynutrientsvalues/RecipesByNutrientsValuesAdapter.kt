package com.example.diegopizzo.foodietest.ui.recipesbynutrientsfragment.recipesbynutrientsvalues

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.business.network.datamodel.RecipeByNutrients
import kotlinx.android.synthetic.main.recipes_by_nutrients_values_item_layout.view.*

class RecipesByNutrientsValuesAdapter(private val listener: OnItemClickListener) :
    RecyclerView.Adapter<RecipesByNutrientsValuesAdapter.ViewHolder>() {

    private val recipesByNutrientsList: MutableList<RecipeByNutrients> = mutableListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context: Context = parent.context
        val inflater = LayoutInflater.from(context)
        val view = inflater.inflate(R.layout.recipes_by_nutrients_values_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return recipesByNutrientsList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val recipeByNutrients = recipesByNutrientsList[position]
        holder.itemView.recipeItemCardByNutrients
            .setRecipeByNutrients(
                recipeByNutrients.title,
                recipeByNutrients.image,
                recipeByNutrients.calories,
                recipeByNutrients.carbs,
                recipeByNutrients.protein,
                recipeByNutrients.fat
            )

        holder.setOnClickListener(recipeByNutrients.id)
    }

    fun addListIntoAdapter(recipesByNutrientsListToAdd: List<RecipeByNutrients>) {
        recipesByNutrientsList.clear()
        recipesByNutrientsList.addAll(recipesByNutrientsListToAdd)
        notifyDataSetChanged()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun setOnClickListener(id: Int) {
            itemView.setOnClickListener {
                listener.onItemClick(id)
            }
        }
    }

    interface OnItemClickListener {
        fun onItemClick(id: Int)
    }
}