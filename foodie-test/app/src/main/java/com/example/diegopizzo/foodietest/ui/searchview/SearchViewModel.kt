package com.example.diegopizzo.foodietest.ui.searchview

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.diegopizzo.foodietest.business.db.repository.RecipeRepository
import com.example.diegopizzo.foodietest.business.network.datamodel.ResponseResult
import com.example.diegopizzo.foodietest.business.network.datamodel.Result
import com.example.diegopizzo.foodietest.business.network.interactor.FoodieInteractor
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

class SearchViewModel(private val interactor: FoodieInteractor, private val repository: RecipeRepository) :
    ViewModel() {

    private lateinit var disposable: Disposable
    val recipesListMutableLiveData: MutableLiveData<List<Result>> = MutableLiveData()

    fun getRecipesByQuery(query: String) {
        //TODO trovare un modo per non mettere la stringa vuota
        val recipesSingle = interactor
            .searchRecipes(query, "", "", "", loadIntolerances(), number = NUMBER_OF_RECIPES, offset = 0)
        disposable = recipesSingle
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onStart() }
            .doFinally { onEnd() }
            .subscribe(
                { t1: ResponseResult? -> onSuccess(t1) },
                { throwable: Throwable? -> Log.i("Error", throwable?.message) }
            )
    }

    private fun onStart() {
    }

    private fun onEnd() {
    }

    private fun loadIntolerances(): String {
        val intolerances = repository.loadIntolerances()
        return intolerances.joinToString(COMMA_SEPARATE) { t -> t.name.intolerance }
    }

    private fun onSuccess(recipesResult: ResponseResult?) {
        recipesListMutableLiveData.value = recipesResult?.results
    }

    override fun onCleared() {
        super.onCleared()
        //TODO kotlin.UninitializedPropertyAccessException: lateinit property disposable has not been initialized
        disposable.dispose()
    }

    companion object {
        private const val NUMBER_OF_RECIPES = 10
        private const val COMMA_SEPARATE = ","
    }
}