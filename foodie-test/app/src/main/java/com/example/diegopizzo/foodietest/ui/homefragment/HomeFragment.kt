package com.example.diegopizzo.foodietest.ui.homefragment

import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import com.example.diegopizzo.foodietest.databinding.HomeFragmentLayoutBinding
import com.example.diegopizzo.foodietest.ui.recipedetailsactivity.RecipeDetailsActivity
import com.example.diegopizzo.foodietest.ui.utils.*
import kotlinx.android.synthetic.main.recipe_item_extended_view_layout.view.*
import org.koin.android.viewmodel.ext.android.viewModel

class HomeFragment : Fragment() {

    private lateinit var binding: HomeFragmentLayoutBinding
    private val viewModel by viewModel<HomeViewModel>()
    private lateinit var recommendedRecipesAdapter: HomeRecyclerViewAdapter
    private lateinit var popularRecipesAdapter: HomeRecyclerViewAdapter

    companion object {
        const val TAG_HOME_FRAGMENT = "homeFragment"

        fun newInstance(bundle: Bundle?): HomeFragment {
            val homeFragment = HomeFragment()
            if (bundle != null) {
                homeFragment.arguments = bundle
            }
            return homeFragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        recommendedRecipesAdapter =
            HomeRecyclerViewAdapter(context!!, object : HomeRecyclerViewAdapter.OnItemClickListener {
                override fun onItemClick(id: Int) {
                    startRecipeDetailsActivity(id)
                }

            })
        popularRecipesAdapter =
            HomeRecyclerViewAdapter(context!!, object : HomeRecyclerViewAdapter.OnItemClickListener {
                override fun onItemClick(id: Int) {
                    startRecipeDetailsActivity(id)
                }

            })
    }


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment_layout, container, false)
        binding.viewModel = viewModel

        setPopularRecipesRecyclerView()
        setRecommendedRecipesRecyclerView()
        callAPI()
        viewModel.recipesPopularListMutableLiveData.observe(this, Observer { recipeList ->
            popularRecipesAdapter.addItemsIntoTheList(recipeList)
        })

        viewModel.recipesRecommendedListMutableLiveData.observe(this, Observer { recipeList ->
            recommendedRecipesAdapter.addItemsIntoTheList(recipeList)
        })

        viewModel.recipeMutableLiveData.observe(this, Observer { recipe ->
            setCardRecipeOfTheDay(recipe)
        })
        return binding.root
    }

    private fun setCardRecipeOfTheDay(recipe: Recipe) {
        binding.extendedRecipeItemView.titleItemRecipeExtended.text = recipe.title
        binding.extendedRecipeItemView.instructionsTitleItemRecipeExtended.text = RECIPE_INSTRUCTIONS
        Glide.with(this).load(recipe.image).apply(glideOptions(context!!))
            .into(binding.extendedRecipeItemView.imageItemRecipeExtended)
        binding.extendedRecipeItemView.instructionsValueItemRecipeExtended.text =
            if (!TextUtils.isEmpty(recipe.instructions)) instructionsTruncate(removeHtmlTags(recipe.instructions!!))
            else RECIPE_INSTRUCTIONS_NO_VALUE
        binding.extendedRecipeItemView.recipeExtendedItemCard.setOnClickListener {
            startRecipeDetailsActivity(recipe.id)
        }
    }

    private fun setPopularRecipesRecyclerView() {
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.popularRecipesRecyclerView.layoutManager = layoutManager
        binding.popularRecipesRecyclerView.adapter = popularRecipesAdapter
    }

    private fun setRecommendedRecipesRecyclerView() {
        val layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        binding.recommendedRecipesRecyclerView.layoutManager = layoutManager
        binding.recommendedRecipesRecyclerView.adapter = recommendedRecipesAdapter
    }

    private fun callAPI() {
        viewModel.getPopularRecipes()
        viewModel.getRecommendedRecipes()
        viewModel.getRandomRecipe()
    }

    private fun startRecipeDetailsActivity(id: Int) {
        val intent =
            Intent(context, RecipeDetailsActivity::class.java).putExtra(RecipeDetailsActivity.RECIPE_ID_BUNDLE_KEY, id)
        startActivity(intent)
    }
}