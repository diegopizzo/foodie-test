package com.example.diegopizzo.foodietest.ui.recipesbynutrientsfragment.recipesbynutrientssetter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.ui.customview.NutrientSetterItemView
import com.example.diegopizzo.foodietest.ui.utils.Nutrients
import kotlinx.android.synthetic.main.recipes_by_nutrients_setter_item_layout.view.*

class RecipesByNutrientsSetterAdapter(private val nutrientSetterList: List<Nutrients>) :
    RecyclerView.Adapter<RecipesByNutrientsSetterAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context: Context = parent.context
        val inflater: LayoutInflater = LayoutInflater.from(context)
        val view: View = inflater.inflate(R.layout.recipes_by_nutrients_setter_item_layout, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return nutrientSetterList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val nutrientItemView = holder.itemView.nutrientItemView
        nutrientItemView.setOnChangeDefaultValueListener(object : NutrientSetterItemView.OnChangeDefaultValueListener {
            override fun onChangeDefaultValue(value: Int) {
                updateDefaultValueOfNutrient(position, value)
                notifyItemChanged(position)
            }
        })
        val nutrientItem = nutrientSetterList[position]
        nutrientItemView.setNutrient(nutrientItem.nutrientName)
        nutrientItemView.setNutrientSeekBar(nutrientItem.defaultValue, nutrientItem.maxValue, nutrientItem.minValue)
    }

    fun getListOfNutrients(): List<Nutrients> {
        return nutrientSetterList
    }

    private fun updateDefaultValueOfNutrient(position: Int, value: Int) {
        val nutrientItem = nutrientSetterList[position]
        nutrientItem.defaultValue = value
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}