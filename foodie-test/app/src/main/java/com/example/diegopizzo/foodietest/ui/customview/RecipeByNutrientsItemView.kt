package com.example.diegopizzo.foodietest.ui.customview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.cardview.widget.CardView
import com.bumptech.glide.Glide
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.ui.utils.glideOptions
import kotlinx.android.synthetic.main.recipe_item_nutrients_view_layout.view.*

class RecipeByNutrientsItemView(context: Context, attrs: AttributeSet?) : CardView(context, attrs) {

    init {
        View.inflate(context, R.layout.recipe_item_nutrients_view_layout, this)
    }

    fun setRecipeByNutrients(
        title: String,
        image: String?,
        calories: Int,
        carbs: String,
        proteins: String,
        fats: String
    ) {
        titleItemRecipeNutrients.text = title
        Glide.with(this).load(image).apply(glideOptions(context)).into(imageItemRecipeByNutrients)
        caloriesValueRecipeTextView.text = calories.toString()
        carbsValueRecipeTextView.text = carbs
        proteinsValueRecipeTextView.text = proteins
        fatsValueRecipeTextView.text = fats
    }
}