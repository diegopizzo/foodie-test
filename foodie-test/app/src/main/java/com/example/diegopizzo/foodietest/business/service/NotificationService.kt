package com.example.diegopizzo.foodietest.business.service

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.preference.PreferenceManager
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.ui.MainActivity
import com.example.diegopizzo.foodietest.ui.foodietestfragment.FoodieTestFragment
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage


class NotificationService : FirebaseMessagingService() {

    companion object {
        private const val TAG = "MyFirebaseIIDService"
        private const val TAG_TOKEN = "myToken"
        private const val FOODIE_CHANNEL_ID = "foodieChannel"
        private const val FOODIE_NOTIFICATION_ID = 10000
    }

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {

        createNotificationChannel()

        // Create an explicit intent for an Activity in your app
        val intent = Intent(this, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            putExtra(FoodieTestFragment.TAG_FOODIE_TEST_FRAGMENT, FoodieTestFragment.TAG_FOODIE_TEST_FRAGMENT)
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)

        val notificationBuilder = NotificationCompat.Builder(this, FOODIE_CHANNEL_ID)
            .setSmallIcon(R.drawable.ic_local_dining_black_24dp)
            .setContentTitle(remoteMessage!!.data["title"]) //the "title" value you sent in your notification
            .setContentText(remoteMessage.data["message"])
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

        with(NotificationManagerCompat.from(this)) {
            // notificationId is a unique int for each notification that you must define
            notify(FOODIE_NOTIFICATION_ID, notificationBuilder.build())
        }
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of
     * the previous token had been compromised. Note that this is called when the InstanceID token
     * is initially generated so this is where you would retrieve the token.
     */
    override fun onNewToken(token: String?) {
        Log.d(TAG, "Refreshed token: " + token!!)

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
        val preferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)
        preferences.edit().putString(TAG_TOKEN, token).apply()

    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.foodie_channel_name)
            val descriptionText = getString(R.string.foodie_channel_description)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(FOODIE_CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}
