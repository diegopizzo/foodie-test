package com.example.diegopizzo.foodietest.business.converter

import androidx.room.TypeConverter
import com.example.diegopizzo.foodietest.ui.utils.Intolerances

class IntolerancesConverter {
    companion object {

        @TypeConverter
        @JvmStatic
        fun fromIntolerances(value: Intolerances): String {
            return value.intolerance
        }

        @TypeConverter
        @JvmStatic
        fun toIntolerances(value: String): Intolerances {
            return Intolerances.valueOf(value.toUpperCase())
        }
    }
}