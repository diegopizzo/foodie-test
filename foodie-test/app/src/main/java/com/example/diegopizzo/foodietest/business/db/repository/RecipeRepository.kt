package com.example.diegopizzo.foodietest.business.db.repository

import com.example.diegopizzo.foodietest.business.db.entity.IntoleranceEntity
import com.example.diegopizzo.foodietest.business.db.entity.RecipeEntity
import com.example.diegopizzo.foodietest.ui.utils.Intolerances
import io.reactivex.Completable
import io.reactivex.Single

interface RecipeRepository {

    fun insertAll(vararg recipeEntities: RecipeEntity): Completable

    fun insertAll(vararg intoleranceEntity: IntoleranceEntity): Completable

    fun insertByIntoleranceName(name: Intolerances)

    fun loadAllRecipesLiked(requestedLoadSize: Int): List<RecipeEntity>

    fun loadAllRecipesLikedAfter(key: Int, requestedLoadSize: Int): List<RecipeEntity>

    fun loadAllRecipesNotLiked(requestedLoadSize: Int): List<RecipeEntity>

    fun loadAllRecipesNotLikedAfter(key: Int, requestedLoadSize: Int): List<RecipeEntity>

    fun loadIntolerances(): List<IntoleranceEntity>

    fun loadIntoleranceByName(intolerances: Intolerances): Single<IntoleranceEntity>

    fun deleteIntoleranceByName(name: Intolerances)
}