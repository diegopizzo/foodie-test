package com.example.diegopizzo.foodietest.ui.recipeslistfragment.likedrecipesfragment

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.paging.LivePagedListBuilder
import com.example.diegopizzo.foodietest.business.db.datasource.RecipeDataSourceLikedFactory
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import com.example.diegopizzo.foodietest.ui.utils.pagedListConfig
import io.reactivex.disposables.Disposable

class LikedRecipesViewModel(dataSourceFactory: RecipeDataSourceLikedFactory) : ViewModel() {

    private lateinit var disposable: Disposable
    val progressBarVisibility: MutableLiveData<Int> = MutableLiveData()
    val likedRecipesImageViewVisibility: MutableLiveData<Int> = MutableLiveData()

    val likedRecipesListLiveData =
        LivePagedListBuilder<Int, Recipe>(dataSourceFactory, pagedListConfig).build()


    private fun onStart() {
        progressBarVisibility.value = View.VISIBLE
    }

    private fun onEnd() {
        progressBarVisibility.value = View.GONE
    }

    private fun onSuccess(likedRecipes: List<Recipe>) {
        likedRecipesImageViewVisibility.value = View.GONE
    }

    private fun onError() {
        likedRecipesImageViewVisibility.value = View.VISIBLE
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }
}