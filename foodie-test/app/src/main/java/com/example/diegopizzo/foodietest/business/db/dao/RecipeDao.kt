package com.example.diegopizzo.foodietest.business.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.diegopizzo.foodietest.business.db.entity.IntoleranceEntity
import com.example.diegopizzo.foodietest.business.db.entity.RecipeEntity
import com.example.diegopizzo.foodietest.ui.utils.Intolerances
import io.reactivex.Completable
import io.reactivex.Single

@Dao
interface RecipeDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg recipeEntities: RecipeEntity): Completable

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertAll(vararg intoleranceEntity: IntoleranceEntity): Completable

    @Query("INSERT INTO intolerance values(null,:name)")
    fun insertByIntoleranceName(name: Intolerances)

    @Query("SELECT * FROM recipe where recipe.is_liked = '1' ORDER BY id_recipe asc limit :requestedLoadSize")
    fun loadAllRecipesLiked(requestedLoadSize: Int): List<RecipeEntity>

    @Query("SELECT * FROM recipe where recipe.is_liked = '1' and id_recipe > :key ORDER BY id_recipe asc limit :requestedLoadSize")
    fun loadAllRecipesLikedAfter(key: Int, requestedLoadSize: Int): List<RecipeEntity>

    @Query("SELECT * FROM recipe where recipe.is_liked = '0' ORDER BY id_recipe asc limit :requestedLoadSize")
    fun loadAllRecipesNotLiked(requestedLoadSize: Int): List<RecipeEntity>

    @Query("SELECT * FROM recipe where recipe.is_liked = '0' and id_recipe > :key ORDER BY id_recipe asc limit :requestedLoadSize")
    fun loadAllRecipesNotLikedAfter(key: Int, requestedLoadSize: Int): List<RecipeEntity>

    @Query("SELECT * FROM intolerance")
    fun loadIntolerances(): List<IntoleranceEntity>

    @Query("SELECT * FROM intolerance WHERE name=:intolerance")
    fun loadIntoleranceByName(intolerance: Intolerances): Single<IntoleranceEntity>

    @Query("DELETE FROM intolerance where name=:name")
    fun deleteIntoleranceByName(name: Intolerances)
}