package com.example.diegopizzo.foodietest.business.db.datasource

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import io.reactivex.disposables.CompositeDisposable

class RecipeItemKeyedDataSourceLiked : RecipeItemKeyedDataSourceBase() {

    private val disposable: CompositeDisposable = CompositeDisposable()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Recipe>) {
        val items = recipeRepository.loadAllRecipesLiked(requestedLoadSize = params.requestedLoadSize)
        disposable.add(createRecipesSingle(items).subscribe({ recipes -> callback.onResult(recipes) }, {}))
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Recipe>) {
        val items =
            recipeRepository.loadAllRecipesLikedAfter(key = params.key, requestedLoadSize = params.requestedLoadSize)
        disposable.add(createRecipesSingle(items).subscribe({ recipes -> callback.onResult(recipes) }, {}))
    }

    override fun invalidate() {
        super.invalidate()
        disposable.clear()
    }
}

class RecipeDataSourceLikedFactory(private val dataSource: RecipeItemKeyedDataSourceLiked) :
    DataSource.Factory<Int, Recipe>() {

    private val recipesLiveData = MutableLiveData<RecipeItemKeyedDataSourceLiked>()

    override fun create(): DataSource<Int, Recipe> {
        recipesLiveData.postValue(dataSource)
        return dataSource
    }
}