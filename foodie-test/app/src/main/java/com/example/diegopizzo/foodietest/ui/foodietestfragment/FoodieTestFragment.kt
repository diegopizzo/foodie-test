package com.example.diegopizzo.foodietest.ui.foodietestfragment

import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import com.example.diegopizzo.foodietest.databinding.FoodieTestFragmentLayoutBinding
import com.example.diegopizzo.foodietest.ui.customview.FoodieCard
import com.example.diegopizzo.foodietest.ui.utils.CardUtils
import com.mindorks.placeholderview.SwipeDecor
import com.mindorks.placeholderview.SwipePlaceHolderView
import com.mindorks.placeholderview.SwipeViewBuilder
import kotlinx.android.synthetic.main.foodie_test_fragment_layout.*
import org.koin.android.viewmodel.ext.android.viewModel


class FoodieTestFragment : Fragment() {

    private val viewModel by viewModel<FoodieTestViewModel>()
    private lateinit var binding: FoodieTestFragmentLayoutBinding

    companion object {
        const val TAG_FOODIE_TEST_FRAGMENT = "foodieTestFragment"

        private const val LAST_CARD_TITLE = "Congratulations, test complete!"


        val lastCardRecipe: Recipe =
            Recipe(null, 0, LAST_CARD_TITLE, null, null)

        fun newInstance(bundle: Bundle?): FoodieTestFragment {
            val foodieTestFragment = FoodieTestFragment()
            if (bundle != null) {
                foodieTestFragment.arguments = bundle
            }
            return foodieTestFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.foodie_test_fragment_layout, container, false)
        binding.viewModel = viewModel


        setSwipeView()
        setFabs()
        callAPI()

        viewModel.recipesListMutableLiveData.observe(this,
            Observer { recipeList ->
                if (recipeList != null) {
                    for (recipe: Recipe in recipeList) {
                        binding.swipeView.addView(
                            FoodieCard(context!!, recipe, false)
                        )
                    }
                    binding.swipeView.addView(
                        FoodieCard(
                            context!!, lastCardRecipe, true,
                            object : FoodieCard.OnFragmentInteractorListener {
                                override fun removeClickListener() {
                                    binding.likeFab.setOnClickListener(null)
                                    binding.rejectFab.setOnClickListener(null)
                                    binding.swipeView.lockViews()
                                }
                            })
                    )
                }
            })

        return binding.root

    }


    private fun setSwipeView() {
        val bottomMargin = CardUtils.dpToPx(160)
        val windowSize = CardUtils.getDisplaySize(activity!!.windowManager)
        binding.swipeView.getBuilder<SwipePlaceHolderView, SwipeViewBuilder<SwipePlaceHolderView>>()
            .setDisplayViewCount(3)
            .setSwipeDecor(
                SwipeDecor()
                    .setPaddingTop(20)
                    .setSwipeMaxChangeAngle(2f)
                    .setRelativeScale(0.01f)
                    .setViewWidth(windowSize.x)
                    .setViewHeight(windowSize.y - bottomMargin)
                    .setViewGravity(Gravity.TOP)
            )
    }


    private fun setFabs() {
        binding.likeFab.setOnClickListener { swipeView.doSwipe(true) }
        binding.rejectFab.setOnClickListener { swipeView.doSwipe(false) }
    }

    private fun callAPI() {
        viewModel.getRandomRecipes()
    }

}