package com.example.diegopizzo.foodietest.ui.customview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import com.example.diegopizzo.foodietest.R

class NoRecipesMakeFoodieTestImageView(context: Context, attrs: AttributeSet?) : ImageView(context, attrs) {

    init {
        setImageResource(R.drawable.no_recipes_make_food_test)
        visibility = View.GONE
        scaleType = ImageView.ScaleType.FIT_START
    }
}