package com.example.diegopizzo.foodietest.ui.recipeslistfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.databinding.RecipesListFragmentLayoutBinding
import com.example.diegopizzo.foodietest.ui.recipeslistfragment.likedrecipesfragment.LikedRecipesFragment
import com.example.diegopizzo.foodietest.ui.recipeslistfragment.notlikedrecipesfragment.NotLikedRecipesFragment
import com.example.diegopizzo.foodietest.ui.utils.ZoomOutPageTransformer


class RecipesListFragment : Fragment() {

    private lateinit var binding: RecipesListFragmentLayoutBinding

    companion object {
        private const val NUM_PAGES = 2
        const val TAG_RECIPES_LIST_FRAGMENT = "recipesListFragment"

        fun newInstance(bundle: Bundle?): RecipesListFragment {
            val recipesListFragment = RecipesListFragment()
            if (bundle != null) {
                recipesListFragment.arguments = bundle
            }
            return recipesListFragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding = DataBindingUtil.inflate(inflater, R.layout.recipes_list_fragment_layout, container, false)
        setupViewPager()
        return binding.root
    }

    private fun setupViewPager() {
        binding.viewPager.adapter = RecipesListPagerAdapter(childFragmentManager)
        binding.viewPager.setPageTransformer(true, ZoomOutPageTransformer())
    }

    private inner class RecipesListPagerAdapter(fragmentManager: FragmentManager) :
        FragmentStatePagerAdapter(fragmentManager) {

        override fun getCount(): Int = NUM_PAGES

        override fun getItem(position: Int): Fragment {
            return when (position) {
                LikedRecipesFragment.VIEW_PAGER_POSITION -> LikedRecipesFragment.newInstance(null)
                NotLikedRecipesFragment.VIEW_PAGER_POSITION -> NotLikedRecipesFragment.newInstance(null)
                else -> {
                    LikedRecipesFragment.newInstance(null)
                }
            }
        }

        override fun getPageTitle(position: Int): CharSequence? {
            when (position) {
                LikedRecipesFragment.VIEW_PAGER_POSITION -> return LikedRecipesFragment.VIEW_PAGER_TITLE
                NotLikedRecipesFragment.VIEW_PAGER_POSITION -> return NotLikedRecipesFragment.VIEW_PAGER_TITLE
            }
            return super.getPageTitle(position)
        }


    }
}