package com.example.diegopizzo.foodietest.business.network.service

import com.example.diegopizzo.foodietest.business.network.datamodel.*
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Path
import retrofit2.http.Query

interface FoodieService {

    companion object {
        const val ENDPOINT = "https://spoonacular-recipe-food-nutrition-v1.p.rapidapi.com"
        const val BASE_URI_IMAGES = "https://spoonacular.com/recipeImages/"
        private const val API_KEY_VALUE = "qZ1ZbEAgRymshmSJ5IzdmwP68NzPp1n5bezjsnbd6PLOoY960B"
        private const val API_KEY_NAME = "X-RapidAPI-Key"
    }

    @Headers("Content-Type: application/json", "$API_KEY_NAME: $API_KEY_VALUE")
    @GET("/recipes/search")
    fun searchRecipes(
        @Query("query") query: String, @Query("cuisine") cuisine: String?,
        @Query("diet") diet: String?, @Query("excludeIngredients") excludeIngredients: String?,
        @Query("intolerances") intolerances: String?,
        @Query("number") number: Int?,
        @Query("offset") offset: Int?
    ): Single<ResponseResult>


    @Headers("Content-Type: application/json", "$API_KEY_NAME: $API_KEY_VALUE")
    @GET("/recipes/random")
    fun getRandomRecipes(@Query("number") number: Int): Single<ResponseRecipe>


    @Headers("Content-Type: application/json", "$API_KEY_NAME: $API_KEY_VALUE")
    @GET("/{id}/similar")
    fun getSimilarRecipes(@Path("id") id: Int): Single<Result>

    @Headers("Content-Type: application/json", "$API_KEY_NAME: $API_KEY_VALUE")
    @GET("/recipes/{id}/information")
    fun getRecipeInformationById(@Path("id") id: Int): Single<Recipe>

    @Headers("Content-Type: application/json", "$API_KEY_NAME: $API_KEY_VALUE")
    @GET("/recipes/findByNutrients")
    fun getRecipesByNutrients(
        @Query("minCalories") minCalories: Int,
        @Query("minCarbs") minCarbs: Int,
        @Query("minProtein") minProtein: Int,
        @Query("minFat") minFat: Int,
        @Query("maxCalories") maxCalories: Int,
        @Query("maxCarbs") maxCarbs: Int,
        @Query("maxProtein") maxProtein: Int,
        @Query("maxFat") maxFat: Int
    ): Single<List<RecipeByNutrients>>
}