package com.example.diegopizzo.foodietest.ui.recipedetailsactivity

import android.os.Bundle
import android.text.TextUtils
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.bumptech.glide.Glide
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import com.example.diegopizzo.foodietest.databinding.ActivityRecipeDetailsLayoutBinding
import com.example.diegopizzo.foodietest.ui.utils.RECIPE_INSTRUCTIONS
import com.example.diegopizzo.foodietest.ui.utils.RECIPE_INSTRUCTIONS_NO_VALUE
import com.example.diegopizzo.foodietest.ui.utils.glideOptions
import com.example.diegopizzo.foodietest.ui.utils.removeHtmlTags
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.recipe_item_extended_view_layout.view.*
import org.koin.android.viewmodel.ext.android.viewModel

class RecipeDetailsActivity : AppCompatActivity() {

    private val viewModel by viewModel<RecipeDetailsViewModel>()
    private lateinit var binding: ActivityRecipeDetailsLayoutBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_recipe_details_layout)
        binding.viewModel = viewModel
        val recipeId: Int = intent.getIntExtra(RECIPE_ID_BUNDLE_KEY, 0)
        viewModel.getRecipeDetails(recipeId)
        viewModel.recipeDetailsMutableLiveData.observe(this, Observer { recipe ->
            setRecipeDetails(recipe)
        })

        viewModel.status.observe(this, Observer {
            if (it != null) {
                showMessage(it)
            }
        })
    }

    private fun setRecipeDetails(recipe: Recipe) {
        binding.recipeExtendedRecipeDetails.titleItemRecipeExtended.text = recipe.title
        Glide.with(this).load(recipe.image).apply(glideOptions(this))
            .into(binding.recipeExtendedRecipeDetails.imageItemRecipeExtended)
        binding.recipeExtendedRecipeDetails.instructionsTitleItemRecipeExtended.text = RECIPE_INSTRUCTIONS
        binding.recipeExtendedRecipeDetails.instructionsValueItemRecipeExtended.text =
            if (!TextUtils.isEmpty(recipe.instructions)) removeHtmlTags(recipe.instructions!!) else RECIPE_INSTRUCTIONS_NO_VALUE
    }

    private fun showMessage(message: String) {
        Snackbar.make(binding.root, message, Snackbar.LENGTH_LONG).show()
    }

    companion object {
        const val RECIPE_ID_BUNDLE_KEY = "recipeId"
    }
}