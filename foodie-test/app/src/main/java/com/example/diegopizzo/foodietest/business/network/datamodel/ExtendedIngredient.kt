package com.example.diegopizzo.foodietest.business.network.datamodel

import com.google.gson.annotations.Expose

data class ExtendedIngredient(
    @Expose val id: Int,
    @Expose val image: String,
    @Expose val name: String
)