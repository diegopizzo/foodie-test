package com.example.diegopizzo.foodietest.config.koin

import com.example.diegopizzo.foodietest.business.converter.IntolerancesConverter
import com.example.diegopizzo.foodietest.business.creator.Creators
import org.koin.dsl.module.module

val utilsModule = module {

    //Creator
    single { provideCreator() }

    //Converter
//    single { provideIntolerancesConverter() }

}

fun provideCreator(): Creators {
    return Creators()
}

fun provideIntolerancesConverter(): IntolerancesConverter {
    return IntolerancesConverter()
}