package com.example.diegopizzo.foodietest.ui.customview

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import com.example.diegopizzo.foodietest.R

class RecipeItemView(context: Context, attrs: AttributeSet?) : CardView(context, attrs) {

    init {
        View.inflate(context, R.layout.recipe_item_view_layout, this)
        val recipeImageView: ImageView = findViewById(R.id.imageItemRecipe)
        val titleTextView: TextView = findViewById(R.id.titleItemRecipe)
    }
}