package com.example.diegopizzo.foodietest.business.db.config

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.diegopizzo.foodietest.business.converter.IntolerancesConverter
import com.example.diegopizzo.foodietest.business.db.dao.RecipeDao
import com.example.diegopizzo.foodietest.business.db.entity.IntoleranceEntity
import com.example.diegopizzo.foodietest.business.db.entity.RecipeEntity

@Database(entities = [RecipeEntity::class, IntoleranceEntity::class], version = 1)
@TypeConverters(IntolerancesConverter::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun recipeDao(): RecipeDao
}