package com.example.diegopizzo.foodietest.business.network.datamodel

import com.google.gson.annotations.Expose

data class RecipeByNutrients(
    @Expose val id: Int,
    @Expose val title: String,
    @Expose val image: String,
    @Expose val calories: Int,
    @Expose val protein: String,
    @Expose val fat: String,
    @Expose val carbs: String
)