package com.example.diegopizzo.foodietest.business.creator

import com.example.diegopizzo.foodietest.business.db.entity.RecipeEntity
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class CreatorsTest {

    private val creators = Creators()

    @Test
    fun fromRecipeDataModelToRecipeDbEntityTest() {
        val dataModel = Recipe(null, 12345, "test", null, null)
        val entity = RecipeEntity(id = 12345, isLiked = true, title = "test")
        assertEquals(creators.fromRecipeDataModelToRecipeDbEntity(dataModel, true), entity)
    }
}