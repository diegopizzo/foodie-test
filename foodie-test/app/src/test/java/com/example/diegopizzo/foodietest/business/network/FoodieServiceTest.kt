package com.example.diegopizzo.foodietest.business.network

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import com.example.diegopizzo.foodietest.business.network.service.FoodieService
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import okio.Okio
import org.hamcrest.CoreMatchers.`is`
import org.junit.After
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

@RunWith(JUnit4::class)
class FoodieServiceTest {

    @Rule
    @JvmField
    val instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var service: FoodieService
    private lateinit var mockWebServer: MockWebServer

    @Before
    fun createService() {
        mockWebServer = MockWebServer()
        service = Retrofit.Builder()
            .baseUrl(mockWebServer.url("/"))
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build()
            .create(FoodieService::class.java)
    }

    @After
    fun stopService() {
        mockWebServer.shutdown()
    }

    @Test
    fun checkThePathValueOfRequests() {
        enqueueResponse("search_recipe.json")
        service.searchRecipes("bolognese", null, null, null, null, null, null)
            .test().assertComplete()
        assertThat(mockWebServer.takeRequest().path, `is`("/recipes/search?query=bolognese"))

        enqueueResponse("random_recipes.json")
        service.getRandomRecipes(3).test().assertComplete()
        assertThat(mockWebServer.takeRequest().path, `is`("/recipes/random?number=3"))

        enqueueResponse("recipe_info.json")
        service.getRecipeInformationById(1222).test().assertComplete()
        assertThat(mockWebServer.takeRequest().path, `is`("/recipes/1222/information"))
    }

    @Test
    fun getRecipeInformationByIdTest() {
        enqueueResponse("recipe_info.json")
        val recipeInfo = service.getRecipeInformationById(1222).test()
        recipeInfo.assertComplete()
        recipeInfo.assertValue { t: Recipe -> t.id == 1222 }
        recipeInfo.assertValue { t: Recipe -> t.title == "Crispy Baked Fish" }
    }

    @Test
    fun searchRecipesTest() {
        enqueueResponse("search_recipe.json")
        val recipesResponse =
            service.searchRecipes("bolognese", null, null, null, null, null, null)
                .test().assertComplete()
        recipesResponse.assertComplete()
        recipesResponse.assertValue { t -> t.results.size == 3 }
        recipesResponse.assertValue { t -> t.results[0].id == 1068672 }
        recipesResponse.assertValue { t -> t.results[0].image == "eggplant-bolognese-1068672.jpg" }
    }


    private fun enqueueResponse(fileName: String, headers: Map<String, String> = emptyMap()) {
        val inputStream = javaClass.classLoader
            .getResourceAsStream("api-response/$fileName")
        val source = Okio.buffer(Okio.source(inputStream))
        val mockResponse = MockResponse()
        for ((key, value) in headers) {
            mockResponse.addHeader(key, value)
        }
        mockWebServer.enqueue(mockResponse.setBody(source.readString(Charsets.UTF_8)))
    }
}