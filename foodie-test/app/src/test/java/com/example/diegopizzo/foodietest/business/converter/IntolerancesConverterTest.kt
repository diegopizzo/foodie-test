package com.example.diegopizzo.foodietest.business.converter

import com.example.diegopizzo.foodietest.ui.utils.Intolerances
import org.junit.Assert.assertEquals
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

@RunWith(JUnit4::class)
class IntolerancesConverterTest {

    @Test
    fun fromIntoleranceTest() {
        assertEquals(IntolerancesConverter.fromIntolerances(Intolerances.DAIRY), Intolerances.DAIRY.intolerance)
    }

    @Test
    fun toIntoleranceTest() {
        assertEquals(IntolerancesConverter.toIntolerances(Intolerances.DAIRY.toString()), Intolerances.DAIRY)
    }
}