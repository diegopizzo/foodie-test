package com.example.diegopizzo.foodietest.ui

import android.content.Intent
import android.view.Gravity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.DrawerActions
import androidx.test.espresso.contrib.DrawerMatchers.isClosed
import androidx.test.espresso.contrib.NavigationViewActions
import androidx.test.espresso.matcher.ViewMatchers.isDisplayed
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.ui.foodietestfragment.FoodieTestFragment
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith


@RunWith(AndroidJUnit4::class)
class MainActivityTest {

    @Rule
    @JvmField
    val rule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun checkIfActivityLayoutIsDisplayed() {
        onView(withId(R.id.drawer_layout)).check(matches(isDisplayed()))
        onView(withId(R.id.linear_layout_main_activity)).check(matches(isDisplayed()))
        onView(withId(R.id.toolbar)).check(matches(isDisplayed()))
    }

    @Test
    fun checkIfHomeFragmentIsDisplayedAndFoodieTestFragmentIsNotDisplayed() {
        onView(withId(R.id.home_fragment_layout)).check(matches(isDisplayed()))
        onView(withId(R.id.foodie_test_fragment_layout)).check(doesNotExist())
    }

    @Test
    fun checkIfHomeFragmentIsNotDisplayedAndFoodieTestFragmentIsDisplayed() {
        val i = Intent()
        i.putExtra(FoodieTestFragment.TAG_FOODIE_TEST_FRAGMENT, FoodieTestFragment.TAG_FOODIE_TEST_FRAGMENT)
        rule.launchActivity(i)
        onView(withId(R.id.home_fragment_layout)).check(doesNotExist())
        onView(withId(R.id.foodie_test_fragment_layout)).check(matches(isDisplayed()))
    }

    @Test
    fun checkIfDrawerLayoutOpenAfterClick() {
        openDrawerLayout()
    }

    @Test
    fun checkIfNavigationDrawerReturnFoodieTestFragmentAfterClickItem() {
        openDrawerLayout()
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.foodie_test))
        onView(withId(R.id.foodie_test_fragment_layout)).check(matches(isDisplayed()))
    }

    @Test
    fun checkIfNavigationDrawerReturnRecipeListFragmentAfterClickItem() {
        openDrawerLayout()
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.view_your_list))
        onView(withId(R.id.recipes_list_fragment_layout)).check(matches(isDisplayed()))
    }

    @Test
    fun checkIfNavigationDrawerReturnHomeFragmentAfterClickItem() {
        openDrawerLayout()
        onView(withId(R.id.nav_view)).perform(NavigationViewActions.navigateTo(R.id.home))
        onView(withId(R.id.home_fragment_layout)).check(matches(isDisplayed()))
    }

    private fun openDrawerLayout() {
        onView(withId(R.id.drawer_layout)).check(matches(isClosed(Gravity.LEFT))) // Left Drawer should be closed.
            .perform(DrawerActions.open()) //open the nav drawer
    }
}