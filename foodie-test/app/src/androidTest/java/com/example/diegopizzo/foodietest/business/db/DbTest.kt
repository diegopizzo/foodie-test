package com.example.diegopizzo.foodietest.business.db

import android.content.Context
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.example.diegopizzo.foodietest.business.db.config.AppDatabase
import com.example.diegopizzo.foodietest.business.db.dao.RecipeDao
import com.example.diegopizzo.foodietest.business.db.entity.RecipeEntity
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException


@RunWith(AndroidJUnit4::class)
class DbTest {

    private lateinit var recipeDao: RecipeDao
    private lateinit var database: AppDatabase

    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        recipeDao = database.recipeDao()
    }

    @After
    @Throws(IOException::class)
    fun closeDb() {
        database.close()
    }

    @Test
    @Throws(Exception::class)
    fun writeRecipeAndReadInList() {
        val recipeEntity = RecipeEntity(123344, "title", false)
        recipeDao.insertAll(recipeEntity).test().assertComplete()
        recipeDao.loadAllRecipesNotLiked().test().assertValue { t: List<RecipeEntity> -> t.contains(recipeEntity) }
    }
}