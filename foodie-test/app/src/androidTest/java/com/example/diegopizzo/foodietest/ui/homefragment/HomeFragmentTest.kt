package com.example.diegopizzo.foodietest.ui.homefragment

import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions.scrollTo
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.rule.ActivityTestRule
import com.example.diegopizzo.foodietest.R
import com.example.diegopizzo.foodietest.business.network.datamodel.Recipe
import com.example.diegopizzo.foodietest.business.network.datamodel.ResponseRecipe
import com.example.diegopizzo.foodietest.business.network.interactor.FoodieInteractor
import com.example.diegopizzo.foodietest.testing.SingleFragmentActivity
import io.reactivex.Single
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.koin.android.viewmodel.ext.koin.viewModel
import org.koin.dsl.module.module
import org.koin.standalone.StandAloneContext.loadKoinModules
import org.koin.standalone.StandAloneContext.stopKoin
import org.koin.test.KoinTest
import org.mockito.Mockito
import org.mockito.Mockito.mock


@RunWith(AndroidJUnit4::class)
class HomeFragmentTest : KoinTest {

    private val testActivityRule = ActivityTestRule(SingleFragmentActivity::class.java, true, false)

    lateinit var foodieInteractor: FoodieInteractor
    lateinit var viewModel: HomeViewModel

    companion object {
        private const val TITLE_ONE = "recipe_one"
        private const val TITLE_TWO = "recipe_two"
        private val recipeList: List<Recipe> =
            arrayListOf(
                Recipe(null, 1, TITLE_ONE, null, null),
                Recipe(null, 2, TITLE_TWO, null, null)
            )
        private val responseRecipe = ResponseRecipe(recipeList)
    }

    @Rule
    fun rule() = testActivityRule

    @Before
    fun setup() {
        foodieInteractor = mock(FoodieInteractor::class.java)

        createMockGetRandomRecipes(HomeViewModel.RANDOM_RECIPES_NUMBER, ResponseRecipe(arrayListOf()))

        viewModel = HomeViewModel(foodieInteractor)

        loadKoinModules(module {
            viewModel { viewModel }
        })
        rule().launchActivity(null)
    }

    @After
    fun cleanUp() {
        stopKoin()
    }

    @Test
    fun checkIfTheLayout_IsDisplayed() {
        rule().activity.replaceFragment(HomeFragment.newInstance(null))
        onView(withId(R.id.recommendedRecipesTextView)).check(matches(isDisplayed()))
        onView(withId(R.id.recommendedRecipesRecyclerView)).check(matches(isDisplayed()))
        onView(withId(R.id.popularRecipesTextView)).check(matches(isDisplayed()))
        onView(withId(R.id.popularRecipesRecyclerView)).check(matches(isDisplayed()))
    }

    @Test
    fun checkIfTitlesOfItemsOfRecyclerViewAreDisplayed() {
        onView(withText(TITLE_ONE)).check(doesNotExist())
        onView(withText(TITLE_TWO)).check(doesNotExist())

        createMockGetRandomRecipes(HomeViewModel.RANDOM_RECIPES_NUMBER, responseRecipe)

        launchHomeFragment()

        onView((withId(R.id.popularRecipesRecyclerView))).perform(
            scrollTo<HomeRecyclerViewAdapter.ViewHolder>(
                hasDescendant(withText(TITLE_ONE))
            )
        )

        onView((withId(R.id.popularRecipesRecyclerView))).perform(
            scrollTo<HomeRecyclerViewAdapter.ViewHolder>(
                hasDescendant(withText(TITLE_TWO))
            )
        )
    }


    private fun createMockGetRandomRecipes(id: Int, responseRecipe: ResponseRecipe) {
        Mockito.`when`(foodieInteractor.getRandomRecipes(id))
            .thenReturn(Single.just(responseRecipe))
    }

    private fun launchHomeFragment() {
        rule().activity.replaceFragment(HomeFragment.newInstance(null))
    }
}